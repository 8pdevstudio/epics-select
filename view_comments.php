<div class="galeria-modal-comments">
	<!-- <div class="photo-name">IMG_0021.jpg</div> -->
	<div class="row">
		<div class="col-12 order-1">
			<div class="comment-header px-4 pt-4 pb-0 pb-md-3">
				<div class="custom-control custom-checkbox number number-sm theme-1 label-right">
				  <input type="checkbox" class="custom-control-input" name="categorias" checked="">
				  <label class="custom-control-label"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text">Seleção Álbum</label>
				</div>
				<br>
				<div class="custom-control custom-checkbox number number-sm theme-2 label-right">
				  <input type="checkbox" class="custom-control-input" name="categorias" checked="">
				  <label class="custom-control-label"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text">Seleção Sogra</label>
				</div>
			</div>
		</div>
		<div class="col-12 order-3 order-md-2">
			<ul class="comment-list px-4">
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-3">
							<!-- Se tiver imagem lista aqui -->
							<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-4">
							<!-- Se tiver imagem lista aqui -->
							<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-2">
							<!-- Se tiver imagem lista aqui -->
							<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-3">
							<!-- Se tiver imagem lista aqui -->
							<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia" class="tall">
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-4">
							<!-- Se tiver imagem lista aqui -->
							<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-5">
							<!-- Se tiver imagem lista aqui -->
							<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
				<li class="comment-item d-flex py-3">
					<div class="col-auto pl-0">
						<div class="ratio-img ratio-img-avatar ratio-img-avatar-4">
							<!-- Se tiver imagem lista aqui -->
							<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
							<span>AL</span>
						</div>
					</div>
					<div class="col px-0">
						<strong class="ff-title">Ana Letícia</strong>
						<span class="ml-3 text-muted">1h</span>
						<div class="mt-1 text-muted">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore …</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="col-12 order-2 order-md-3">
			<div class="comment-footer px-4 pb-4 pt-3 pt-md-2">
				<div class="d-flex align-items-center">
					<div class="col px-0">
						<a href="" class="btn btn-square btn-transparent circle mt-0 mb-1"><i class="fa fa-heart text-danger"></i></a>
						<a href="" class="btn btn-square btn-transparent circle mt-0 mb-1 ml-1"><i class="fa fa-shopping-cart"></i></a>
						<a href="" class="btn btn-square btn-transparent circle mt-0 mb-1 ml-1"><i class="fa fa-arrow-to-bottom"></i></a>
						<button class="btn btn-square btn-transparent circle mt-0 mb-1 ml-1" data-toggle="modal" data-target="#modal-compatilhar">
							<i class="fa fa-share"></i>
						</button>
					</div>
					<div class="col-auto px-0">
						<strong class="d-block">72 curtidas</strong>
					</div>
				</div>
				<form id="comentario-modal" class="mt-3">
					<div class="input-group comment comment-lg">
						<span class="ratio-img ratio-img-avatar ratio-img-avatar-3">
							<!-- Se tiver imagem lista aqui -->
							<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia" class="tall">
							<span>AL</span>
						</span>
						<textarea class="form-control" placeholder="Deixe um comentário..." required=""></textarea>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>