$(document).ready(function() {

    $('form.ajax').submit(function(e) {

        e.preventDefault();

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Validação e-mail válido
        var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email   = $(this).find('[name="email"]');

        if (!filter.test(email.val())) {
            email.focus();
            message('danger', 'Insira um e-mail válido');
            return false;
        }
        // - - - - - - - - - - - - - - - - - - - - - - - - - -

        var action  = $(this).attr('action');
        var data    = $(this).serialize();

        $.ajax({
            type: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function(response) {

            if (response.success == true) {
                message('success', response.message);
                window.location.href = response.redirect;
            } else { 
                if(response.message == 'Ops! Email já cadastrado na EPICS') {
                    
                    var email = $('.modal-cadastro input[name="email"]').val();

                    $('.modal-login-email input[name="email"]').val(email);

                    $('.modal-cadastro').fadeOut(function() {
                        $('.modal-login-email').fadeIn();
                    });

                }
                message('danger', response.message);
                return false;
            }

        });

    });

    $('body').on('click', '.btn-login-email', function() {
        $('.modal-login-fb').fadeOut(function() {
            $('.modal-login-email').fadeIn();
        });
    });

    $('body').on('click', '.btn-criar-conta', function() {
        $('.modal-login-fb').fadeOut(function() {
            $('.modal-cadastro').fadeIn();
        });
    });

    $('body').on('click', '.btn-voltar-cadastro', function() {
        $('.modal-cadastro').fadeOut(function() {
            $('.modal-login-fb').fadeIn();
        });
    });

    $('body').on('click', '.btn-voltar-login', function() {
        $('.modal-login-email').fadeOut(function() {
            $('.modal-login-fb').fadeIn();
        });
    });

    // - - - - - -
    //VERIFICA SE O CAMPO DE TELEFONE TERÁ OU NÃO A MÁSCARA
    verificarMascaraTelefone();
    $('#pais_telefone').on('change', function() {
        verificarMascaraTelefone();
    });

<<<<<<< Updated upstream
    $('[data-goto-step]').click(function(e) {

        e.preventDefault();
        var showStep = $(this).attr('data-goto-step');
        var showForm = $(this).attr('data-goto-form');
        console.log(showForm);
        dataStep( showStep, showForm );

    });

=======
>>>>>>> Stashed changes
});

// - - - - - - - - - - - - - - - - - - - - - - - - - -
// REGISTRAR COM FACEBOOK
function FB_Login() {

    FB.login(function(response) {
        
        if (response.authResponse) {

            FB.api('/me?fields=id,email',

                function(response) {

                    var data = {
                        idFacebook  : response.id,
                        email       : response.email,
                        uri         : $('input[name="uri"]').val()
                    };

                    $.ajax({
                        url: base_url('accounts/login_post'),
                        method: 'POST',
                        data: data,
                        dataType: 'json',
                        success: function(response) {
                            if (response.success == true) {
                                message('success', response.message);
                                window.location.href = response.redirect;
                            } else { 
                                message('danger', response.message);
                                return false;
                            }
                        }
                    });

                }, {scope: 'email'}

            );

        } else return false;

    }, {scope: 'email'});

}

// - - - - - - - - - - - - - - - - - - - - - - - - - -
// REGISTRAR COM FACEBOOK
function FB_Register() {

    FB.login(function(response) {
        
        if (response.authResponse) {

            FB.api('/me?fields=id,name,email',

                function(response) {

                   $('#form-register input[name=idFacebook]').val( response.id );
                   $('#form-register input[name=name]').val( response.name );
                   $('#form-register input[name=email]').val( response.email ).attr('readonly', true);

                   $('#form-register .btn-primary').text('Finalizar Cadastro');
                   $('#form-register .btn-secondary').remove();

                   $('.remove-register').fadeOut(250);

                   message('success', 'Complete os dados para finalizar o cadastro');

                }, {scope: 'email'}

            );

        } else return false;

    }, {scope: 'email'});

}

// - - - - - - - - - - - - - - - - - - - - - - - - - -
// LOGIN COM GOOGLE
function Google_Login() {

    var googleUser = {};

    gapi.load('auth2', function(){
        auth2 = gapi.auth2.init({
            client_id: $('input[name="GCI"]').val()+'.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
        });

        auth2.attachClickHandler(document.getElementById('loginGoogleBtn'), {}, function(googleUser) {

            var idToken = googleUser.getAuthResponse().id_token;
            var profile = googleUser.getBasicProfile();

            var data = {
                tokenGoogle : idToken,
                uri         : $('input[name="uri"]').val()
            };

            $.ajax({
                url: base_url('accounts/login_post'),
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(response) {
                    if (response.success == true) {
                        message('success', response.message);
                        window.location.href = response.redirect;
                    } else { 
                        message('danger', response.message);
                        return false;
                    }
                }
            });
            
        }, function(error) {
            console.log(JSON.stringify(error, undefined, 2));
        });
    });

}

function Google_Register() {

    var googleUser = {};

    gapi.load('auth2', function(){
        auth2 = gapi.auth2.init({
            client_id: $('input[name="GCI"]').val()+'.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
        });

        auth2.attachClickHandler(document.getElementById('registerGoogleBtn'), {}, function(googleUser) {

            var profile = googleUser.getBasicProfile();
            var idToken = googleUser.getAuthResponse().id_token;

            $('#form-register input[name=tokenGoogle]').val( idToken );
            $('#form-register input[name=name]').val( profile.getName() );
            
            if( profile.getEmail() )
                $('#form-register input[name=email]').val( profile.getEmail() ).attr('readonly', true);

            $('#form-register .btn-primary').text('Finalizar Cadastro');
            $('#form-register .btn-secondary').remove();

            $('.remove-register').fadeOut(250);

            message('success', 'Complete os dados para finalizar o cadastro');

        }, function(error) {
            console.log(JSON.stringify(error, undefined, 2));
        });
    });

}

<<<<<<< Updated upstream
function dataStep( step, form ) {

    var time = 250;

    if( step == undefined || step <= 0)
        return false;

    // exibe um step (form)
    if( form != undefined && form > 0 ) {
        
        $('[data-form]').slideUp(time);

        setTimeout(function() {

            $('[data-form="' + form + '"]').slideDown(time);

         }, time);

    // exibe um step (div)
    } else {

        $('[data-step]').slideUp(time);

        setTimeout(function() {

            $('[data-step="' + step + '"]').slideDown(time);

        }, time);

    }

}

// - - - - - - - - - - - - - - - - - - - - - - - - - -
function verificarMascaraTelefone() {

    if( $('#pais_telefone').length > 0 ) {

        if ($('#pais_telefone').val() == '55') {

            var SPMaskBehavior = function(val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
            $('#telefone').mask(SPMaskBehavior, spOptions);
        } else {
            $('#telefone').val($('#telefone').val().replace(/[^0-9]/g, ''));
            $('#telefone').mask('999999999999999');
        }

    } else return false;
=======
// - - - - - - - - - - - - - - - - - - - - - - - - - -
function verificarMascaraTelefone() {
    if ($('#pais_telefone').val() == '55') {

        var SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
        $('#telefone').mask(SPMaskBehavior, spOptions);
    } else {
        $('#telefone').val($('#telefone').val().replace(/[^0-9]/g, ''));
        $('#telefone').mask('999999999999999');
    }
>>>>>>> Stashed changes
}