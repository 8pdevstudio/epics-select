/* DOWNLOAD FOTOS - ZIP */
function download_all_zip(selection_id) {

    var data = {
        selection_id:selection_id,
        TokenGaleria: $('input[name="gt"]').first().val()
    };

    $.ajax({
        url: base_url('photos/download_zip'),
        data: data,
        type: 'POST'
    }).done(function(response) {
        $('#modal-ajax').find('.modal-content').html(response);
        $('#modal-ajax').modal('show');
    });

}

// REALIZE GRID
function get_grid() {

    setTimeout(function(e) {

        // monta o grid de imagens
        if ($(window).width() < 992) {
            $('.flex-images').flexImages({
                container: '.item-foto',
                rowHeight: 200,
                truncate: false
            });
        } else {
            $('.flex-images').flexImages({
                container: '.item-foto',
                rowHeight: 320,
                truncate: false
            });
        }
    }, 150);

}

function loading( type ) {

    if( type == 'show' ) {

        $('#loading').fadeIn(250);

    } else {
        
        $('#loading').fadeOut(200);

    }

}

/* GET FOTOS - GRID */
function get_photos(page_start) {

    //NÃO CONTINUA SE NÃO TIVER TOKEN GALERIA
    if ($('input[name="gt"]').length < 1)
        return false;

    post = {};
    post['TokenGaleria'] = $('input[name="gt"]').first().val();

    post['filters'] = {};

    // captura os checkbox
    $('[name^="filter"]:checked').each(function() {
        var key = $(this).attr('name').replace(/filter\[/iy, '').replace(/\]/iy, '');
        var val = $(this).val();
        post['filters'][key] = val;
    });

    // captura valores do text
    $('[name^="filter"][type=text]').each(function() {
        var key = $(this).attr('name').replace(/filter\[/iy, '').replace(/\]/iy, '');
        var val = $(this).val();
        post['filters'][key] = val;
    });

    var element = document.getElementById('grid-main');

    $.ajax({
        type: "POST",
        data: post,
        url: base_url('galleries/view_photos_post/' + page_start),
        beforeSend: function(response) {

            loading('show');
            //element.innerHTML = '<div class="container text-center"><i class="fas fa-circle-notch fa-spin fa-5x"></i></div>';

        }
    }).done(function(response) {

        loading('hide');

        element.innerHTML = response;

        get_grid();

        setTimeout(function(e) {

            // verifica se tem fotos selecionadas
            $('.item-foto').each(function(e) {
                var input       = $(this).find('input[type="checkbox"]').first();
                itemFotoChecked( input.data('id') );
            });

            if ($('input[name="gp"]').val() == 0) {
                $('.protected').remove();
            }


        }, 250);

        $().galeria_custom();
    });

}

//  - - - - - - - - - - - - - - - - - - - - - -
//FUNÇÃO PARA ADICIONAR OU REMOVER ACTIVE SELECTED
function itemFotoChecked(id) {

    el      = $('.item-foto-' + id);

    this_inputs         = el.find('input[type="checkbox"]');

    if(this_inputs.is(':checked')){
        $(el).addClass('active');
    } else {
        $(el).removeClass('active');
    }

}

//  - - - - - - - - - - - - - - - - - - - - - -
//  FUNÇÃO QUE VERIFICA SE O CHECKBOX FOI SELECIONADO
function verifyCheckbox(modal, close) {

    var element = $(modal);
    var checkbox = element.find('input[type="checkbox"]').first();

    if( checkbox.is(':checked') ) {

        if( close != undefined && close )
            element.modal('hide');

    } else {
        message('danger', 'Confirme sua ação para continuar...');
    }

}


window.resize = function(e) {

    get_grid();

}

// ONLOAD
window.onload = function(e) {

    //  - - - - - - - - - - - - - - - - - - - - - -
    // CARREGA GRID FOTOS

    get_photos(0);

    var filterTypePrevious = 0;

    $('body').on('click', 'input[name="filter[type]"]', function(e) {
        if(filterTypePrevious == 1 && $(this).val() == 1) {
            $('#filter-type-0').click();
            filterTypePrevious = 0;
        } else {
            filterTypePrevious = $(this).val();
        }
    });

    $('[name*="filter["]').change(function(e) { // filtros
        get_photos(0);
    });

    $('body').on('click', 'li.page-item', function(e) { // paginação

        e.preventDefault();

        element = $(this).find('a').attr('href');
        element = element.split('/');
        element = element.slice(-1);

        get_photos(element);

    });

    // SE GALERIA FOR PROTEGIDA
    if ($('input[name="gp"]').val() == 1) {
        
        // bloqueia botão direito
        $(document).on({
            "contextmenu": function(e) {
                e.preventDefault();
                return false;
            },
            "mousedown": function(e) {
                if(e.target.type == 'textarea') return;
                e.preventDefault();
                return false;
            },
            "mouseup": function(e) {
                e.preventDefault();
                return false;
            }
        });

        // bloqueia print screen
        $(document).keyup(function(e){
            if(e.keyCode == 44) return false;
        });

        // aviso modal - primeira abertura
        $('body').on('click', '.item-foto', function(e) {

            var idGaleria   = $('input[name="gt"]').first().val();
            var nameCookie  = 'seeProtectedModal' + idGaleria;
            var getCookie   = myGetCookie(nameCookie);

            // se não existir cookies, exibe modal
            if( !getCookie ) {

                $('#modal-protection').modal({backdrop: 'static', keyboard: false});
                mySetCookie(nameCookie, '1', 3);

            } else return false;

        });
        

    }

    //  - - - - - - - - - - - - - - - - - - - - - -
    //GERENCIA SELEÇÃO
    $('body').on('change', 'input[name="ac_selection"]', function(e) {

        var element = $(this);
        var action = element.is(":checked") ? 'insert' : 'delete';
        var selection_id = element.data('selection')

        var data = {
            photo_id: element.data('id'),
            action: action,
            selection_id: selection_id
        };

        // - - -

        $.ajax({
            url: base_url('galleries/view_photos_selection_post'),
            data: data,
            dataType: 'json',
            type: 'POST',
            success: function(response) {

                //get all elements with two params (checked true or false)
                var elements = $('[name="ac_selection"][data-selection="' + data.selection_id + '"][data-id="' + data.photo_id + '"]');

                if (response.success) {

                    var count_element = $('label p#count-selection-' + selection_id);
                    var count_value = parseInt(count_element.text().trim());

                    if (action == 'insert') {
                        count_element.text(count_value + 1);
                        elements.prop('checked', true);
                    } else if (action == 'delete') {
                        count_element.text(count_value - 1);
                        elements.prop('checked', false);
                    } else return false;

                    itemFotoChecked( data.photo_id );

                } else {

                    // VOLTA AO ESTADO INICIAL
                    if (action == 'insert') {
                        element.prop('checked', false);
                    } else {
                        element.prop('checked', true);
                    }

                }

            }
        });

    });

    //  - - - - - - - - - - - - - - - - - - - - - -
    // OCULTA / EXIBE FOTO
    $('body').on('click', 'a.ac_hidden', function(e) {

        var element = $(this);

        var data = {
            idGaleria: $('input[name="gi"]').val(),
            idFoto: element.data('id'),
            action: element.data('action')
        };

        // - - -

        $.ajax({
            url: base_url('galleries/view_photos_hidden_post'),
            data: data,
            dataType: 'json',
            type: 'POST',
            success: function(response) {

                if (response.success) {

                    $('.flex-images .item-foto-' + element.data('id')).remove();

                    //REFAZ O GRID
                    setTimeout(function(e) {
                        get_grid();
                    }, 100);

                } else return false;

            }
        });

    });

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    // CURTE E DESCURTE UMA FOTO
    $('body').on('click', 'a.ac_like', function(e) {

        var element = $(this);
        var css = "text-danger";

        var data = {
            idGaleria: $('input[name="gi"]').val(),
            idFoto: element.data('id'),
            action: element.hasClass(css) ? "delete" : "insert"
        };

        // - - -

        $.ajax({
            url: base_url('galleries/view_photos_like_post'),
            data: data,
            dataType: 'json',
            type: 'POST',
            success: function(response) {

                var elements = $('.ac_like[data-id="' + data.idFoto + '"]');

                var ele_count = $('.count-likes-' + data.idFoto);
                var count = ele_count.text().trim();
                count = parseInt(count);

                if (response.success) {

                    if (elements.hasClass(css)) { //remove a curtida
                        $('.item-foto-'+data.idFoto).removeClass('liked');
                        elements.removeClass(css);
                        ele_count.text(count - 1);
                    } else { // adiciona a curtida
                        $('.item-foto-'+data.idFoto).addClass('liked');
                        elements.addClass(css);
                        ele_count.text(count + 1);
                    }

                } else return false;

            }
        });

    });

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    // ENVIA COMENTÁRIO COM ENTER
    $('body').on('keypress', 'textarea[name="FotoComentario"]', function(e) {
        if (e.which == 13) {
            $('#insert-comment').submit();
        } else return true;
    });

    // ENVIA O COMENTÁRIO - FORM SUBMIT
    $('body').on('submit', '#insert-comment', function(e) {

        e.preventDefault();

        var data = {
            TokenGaleria: $('input[name="gt"]').first().val(),
            idFoto: $(this).find('[name="id"]').first().val(),
            FotoComentario: $(this).find('[name="FotoComentario"]').first().val()
        };

        var element = $('textarea[name="FotoComentario"]');

        element.attr('readonly', true);

        $.ajax({
            url: base_url('comments/insert_post'),
            data: data,
            dataType: 'json',
            type: 'POST',
            success: function(response) {

                if (response.success) {

                    // limpa o elemento se for o primeiro comentário
                    if ($('ul.comment-list li').length <= 0)
                        $('ul.comment-list').html('');

                    $('ul.comment-list').append(response.message).slideDown(250);
                    $('ul.comment-list li').last().hide(0);

                    element.val('');

                } else return false;

            }
        }).done(function(response) {

            element.attr('readonly', false);

        });

    });

    // - - - - - - - - - - - - - - - - - - - - - - - - - 
    // DELETE COMENTÁRIO
    $('body').on('click', '.comment-item-delete', function(e) {

        e.preventDefault();

        var id = $(this).attr('data-id');

        $.ajax({
            url: base_url('comments/delete_get/' + id),
            dataType: 'json',
            success: function(response) {

                if (response.success) {
                    $('#comment-item-' + id).slideUp(250);
                    setTimeout(function(e) {
                        $('#comment-item-' + id).remove();

                        // limpa o elemento se for o primeiro comentário
                        if ($('ul.comment-list li').length <= 0)
                            $('ul.comment-list').html('Seja o primeiro a comentar...');

                    }, 250);
                } else return false;

            }
        });

    });

    // - - - - - - - - - - - - - - - - - - - - - - - - - 
    // MODAL AJAX (CARREGA UMA MODAL DINÂMICA - GET)
    $('body').on('click', '.modal-ajax', function(e) {

        e.preventDefault();

        $.ajax({
            url: $(this).data('href'),
            success: function(response) {

                $('#modal-ajax').modal('show');
                $('#modal-ajax .modal-content').html(response);

            }
        });

    });

    // - - - - - - - - - - - - - - - - - - - - - - - - - 
    // AO FECHAR MODAL REALIZAR GRID
    $('body').on('hidden.bs.modal', '.modal', function() {

        get_grid();

    });

    //  - - - - - - - - - - - - - - - - - - - - - -
    // INICIA FANCYBOX
    window.fancyboxOptions = {
        baseClass: "fancybox-custom-layout",
        infobar: false,
        loop: true,
        touch: {
            vertical: false
        },
        buttons: ["close"],
        animationEffect: "fade",
        transitionEffect: "fade",
        preventCaptionOverlap: false,
        gutter: 0,
        idleTime: false,
        keyboard: false,
        // Customize caption area
        caption: function(instance, current) {

            var id  = new URL(current.src);
            id      = id.searchParams.get('id');
            id      = parseInt(id);

            if (id > 0) {
                return '<div id="fancybox-comments-' + id + '"></div>';
            } else return false;

        },
        onInit: function(instance) {

            $('img.fancybox-image').css('display', 'block');

            setTimeout(function() {
                var count = $('.fancybox-container').length;
                if( count > 1 )
                    $('.fancybox-container:not(:last-child)').remove();
            }, 250);

        },
        beforeLoad: function(instance, slide) {

            // NAVEGAÇÃO INFINITA 
            var itemArrActive   = instance.currIndex;
            var itemArrLast     = instance.group.length - 1;
            var itemArrPrev     = instance.prevIndex;

            var btnNavNext      = $('.page-link[aria-label="Next"]').first();
            var btnNavPrev      = $('.page-link[aria-label="Previous"]').first();

            if( btnNavNext.length > 0 && itemArrActive === 0 && itemArrPrev === itemArrLast ) {

                $('.page-link[aria-label="Next"]').click();
                $('img.fancybox-image').css('display', 'none');
                setTimeout(function() {
                    $('[data-fancybox="galeria"]').first().click();
                }, 250);

            } else if( btnNavPrev.length > 0 && itemArrActive === itemArrLast && itemArrPrev === 0 ) {

                $('.page-link[aria-label="Previous"]').click();
                $('img.fancybox-image').css('display', 'none');
                setTimeout(function() {
                    $('[data-fancybox="galeria"]').last().click();
                }, 250);

            }

            // POPULA COMENTÁRIOS
            var id  = new URL(instance.current.src);
            id      = id.searchParams.get('id');
            id      = parseInt(id);

            if (id > 0) {

                var data = {
                    TokenGaleria: $('input[name="gt"]').val(),
                    idFoto: id
                };

                $.ajax({
                    url: base_url('comments/view_post'),
                    data: data,
                    type: 'POST',
                    success: function(response) {
                        $('#fancybox-comments-' + id).html(response);
                    }
                });

            }

            $(instance.current.$content).find('.protected').remove();
            $(instance.current.$content).prepend('<img src="'+base_url('assets/images/img_2498.png')+'" class="protected">');

        },
        beforeShow: function(instance, slide) {

            if ($(window).width() < 992) {
                
                $('.fancybox-content').addClass('loading');

                setTimeout(function() {
                    var ratio = instance.current.width / $(window).width();
                    ratio = (ratio <= 1 ? 1 : ratio);

                    var newH = instance.current.height / ratio;
                    newH = newH > $(window).height() - 120 ? $(window).height() - 120 : newH;

                    image_is_tall = instance.current.width / instance.current.height < 1 ? true : false
                    if (image_is_tall) {
                        $(instance.current.$content).addClass('image-tall');
                    } else {
                        $(instance.current.$content).removeClass('image-tall');
                    }

                    var img = $(instance.current.$image);

                    if(img.width() > img.height()) {
                        //Horizontal
                        img.addClass('image-horizontally');
                    } else {
                        //Vertical
                        img.addClass('image-vertically');
                    }

                    setTimeout(function() {
                        if(img.width() > img.height()) {
                            //Horizontal
                            var newTopImg = img.height() / 2;
                            img.attr('style', 'top: calc(50% - '+newTopImg+'px)');
                        } else {
                            //Vertical
                            var newLeftImg = img.width() / 2;
                            img.attr('style', 'left: calc(50% - '+newLeftImg+'px)');
                        }
                        
                        $('.fancybox-content').removeClass('loading');
                    
                    }, 300);

                    // $('.fancybox-caption').css('top', newH);
                    // var newTopButtons = (newH / 2) - 50;
                    // $('.fancybox-navigation .fancybox-button').attr('style', 'top: '+newTopButtons+'px!important');
                }, 100);
            }

        },
        beforeClose: function() {

            setTimeout(function() {
                get_grid();
            }, 250);

        }

    }

    //  - - - - - - - - - - - - - - - - - - - - - -
    // SOBE A PÁGINA AO CLICAR NO LINK DE PAGINAÇÃO
    $('body').on('click', '.page-link', function() {
        $('html, body').animate({ scrollTop: $('#filtros').offset().top }, 300);
    });

}