//  - - - - - - - - - - - - - - - - - - - - - -
// ABRE MODAL DO CARRINHO E CRIA/BUSCA CESTA
$('body').on('click', '.cart-btn', function() {
    var id          = $(this).data('id');
    var imgPhoto    = $('.item-foto-'+id).find('a').attr('data-href');
    var modalCart   = $('#modal-ajax');

    var data = {
        TokenGaleria: $('input[name="gt"]').val(),
        idGaleria: $('input[name="gi"]').val(),
        idGroupStore: $('input[name="gs"]').val(),
        idFoto: id
    };

    $.ajax({
        url: base_url('cart/view_products_post'),
        data: data,
        type: 'POST'
    }).done(function(response) {
        modalCart.find('.modal-content').html(response);
        modalCart.find('img').attr('src', imgPhoto);
        modalCart.find('.finish-btn').attr('data-id', id);

        modalCart.modal('show');
    });
});

//  - - - - - - - - - - - - - - - - - - - - - -
// ADICIONA OU REMOVE PRODUTOS DO CARRINHO
$('body').on('click', '.finish-btn', function() {
    var idFoto      = $(this).data('id');
    var idCesta     = $(this).data('cestaid');
    var idProdutos  = [];
    var qtdProduto  = [];

    $(this).closest('.modal-body').find('.product-qtd').each(function(index, value) {
        idProdutos.push( $(this).data('id') );
        qtdProduto.push( $(this).val() );
    });

    if(idProdutos.length > 0) {
        var data = {
            idFoto: idFoto,
            idCesta: idCesta,
            idProdutos: idProdutos,
            qtdProduto: qtdProduto
        };

        $.ajax({
            url: base_url('cart/add_products_post'),
            data: data,
            type: 'POST'
        }).done(function(response) {
            
            var response = JSON.parse(response);

            if(response.success) {

                // atualiza itens no carrinho
                $('.cart-quantity').text( response.message.quantity );
                
                if($('input[name="hs"]').val() == '') {
                    
                    var valorTotal = response.message.ValorTotal;

                    if(valorTotal > 0) {
                        $('.btn-finalizar-carrinho').removeClass('d-none');
                    } else {
                        $('.btn-finalizar-carrinho').addClass('d-none');
                    }
                }
            }
        });
    }
});

$('#modal-ajax').on('change', '.product-qtd', function(event) {

    var input_number    = $(this);
    var is_cart         = input_number.hasClass('qtd-cart');
    var new_val         = input_number.val();

    if(is_cart && new_val >= 0) {
        var valor_item  = input_number.data('valor'),
            valor_total = parseFloat(valor_item * new_val).toFixed(2),
            span_total  = input_number.closest('tr').find('.valor-total');

        span_total.html(valor_total.toString().replace('.', ','));
    }

});

$('#modal-ajax').on('click', '.input-number button', function(event) {
    var button_is_plus  = ($(this).hasClass('btn-plus') ? true : false),
        input_number    = $(this).parent().siblings('input[type="number"]'),
        max_val         = input_number.attr('max'),
        new_val         = (button_is_plus ? Number(input_number.val()) + 1 : Number(input_number.val()) - 1);

    if(new_val <= max_val || max_val == undefined) {
        input_number.val((new_val >= 0 ? new_val : 0)).change();
    }
});

$('#modal-ajax').on('focus', '.product-qtd', function(e) {
    $(this).on('wheel', function(e) {
        e.preventDefault();
    });
});

$('#modal-ajax').on('keydown', '.product-qtd', function(e) {
    if ( e.which == 38 || e.which == 40 )
        e.preventDefault();
}); 