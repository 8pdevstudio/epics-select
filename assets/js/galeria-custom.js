$(document).ready(function(){
	galeria_custom_load();
});

var scroll_cache = 0;

function galeria_custom_load(){	
	construct_modal();

	// evento de prev/next
	$('.galeria-modal-body .galeria-modal-comments').addClass('changing');

	// abre modal de visualização
		$('body').on('click', '[data-galeria-custom]', function(){
			toggle_modal('open');

			var element = $(this);
			var id      = element.attr('data-galeria-custom');
			var src     = element.attr('data-galeria-src');

			load_item(id, src);

			return false;
		});
	// -- fim abre modal

	// ativa autoplay
		$("body").on("click", "[data-autoplay-galeria]", function(){

			$(".galeria-modal").addClass("autoplay");

			$("[data-galeria-custom]").first().trigger("click");

			autoplay_control("play");
		});
	// -- fim autoplay

	// pausa autoplay
		$("body").on("click", ".galeria-play", function(){
			autoplay_control("play");
		});
	// -- fim pause

	// pausa autoplay
		$("body").on("click", ".galeria-pause", function(){
			autoplay_control("pause");
		});
	// -- fim pause

	// fecha modal de visualização
		$('body').on('click', '.galeria-modal-close', function(){		
			toggle_modal('close');
		});
	// -- fim fecha modal

	// start autoplay
		setInterval(function(){
			$('body.galeria-modal-open .galeria-modal.autoplay:not(.paused) button.galeria-next').trigger('click');
		}, 7000);
	// -- fim autoplay
}

window.onresize = function(){
	resize_comment_list();
};

// eventos para atalhos
document.onkeydown = function(e){

	switch (e.keyCode) {
        case 37:
            transition_arrow_click('prev');
            break;
        case 38:
            up_arrow_click();
            break;
        case 83:
            up_arrow_click();
            break;
        case 39:
            transition_arrow_click('next');
            break;
        case 40:
            down_arrow_click();
            break
		case 27:
            toggle_modal('close');
            break;
        case 32:
        	e.preventDefault();
        	var pause_play = ($(".galeria-modal.autoplay").hasClass("paused")) ? "play" : "pause";
        	autoplay_control(pause_play);
        	break
    }
};
// -- fim atalhos

// redefine altura da area de listagem dos comentarios
function resize_comment_list(){

	var height_window = $(window).height();
	var height_comment_footer = $('.comment-footer').outerHeight();
	var height_comments_header = $('.comment-header').outerHeight();

	var comment_list = height_window - height_comment_footer - height_comments_header;

	$('.comment-list').height(comment_list);
}

// evento de abrir r fechar modal
function toggle_modal(acao){

	if(acao == 'open'){
		scroll_cache = $(window).scrollTop();

		$("html").attr("scroll", "no");

		$('body').addClass('galeria-modal-open');
		$('.galeria-modal').css('z-index', '99');
	}
	else{
		$(".galeria-carousel .item").removeClass("active");
		$(".galeria-modal").removeClass("autoplay");
		$('body').removeClass('galeria-modal-open');
		$(window).scrollTop(scroll_cache);

		setTimeout(function(){
			$('.galeria-modal').css('z-index', '-10');
		}, 200);
	}		
}

// evento de click seta up
function up_arrow_click(){

	if(!$(':focus').hasClass('form-control') && $('body').hasClass('galeria-modal-open')){
		$('.galeria-modal-body').find('input[type="checkbox"]').first().trigger('click');
	}
}

// evento de click seta down
function down_arrow_click(){
	
	if(!$(':focus').hasClass('form-control')){
		console.log('down');
	}
}

// evento de click seta left/right
function transition_arrow_click(side){

	if(!$('.galeria-modal').hasClass('lock') && $('body').hasClass('galeria-modal-open')){
		$('button.galeria-'+side).trigger('click');
	}
}

function autoplay_control(action){

	if( $('body').hasClass('galeria-modal-open') ){

		if(action == "play"){
			$(".galeria-modal.autoplay").removeClass("paused");
		}
		else{
			$(".galeria-modal.autoplay").addClass("paused");
		}

	}
}

function load_item(id, src){

	if(!$(':focus').hasClass('form-control') && !$('.galeria-modal').hasClass('lock')){

		$('.galeria-modal').addClass('lock');
		$.ajax({
			url: 'async.php',
			data: {id: id},
			type: 'POST',
			dataType: 'json',
			beforeSend: function(){
				construct_item(id, src, true);
				$('.galeria-modal-body').addClass('changing');
			},
			success: function(response){
				$('.galeria-modal-body').removeClass('changing');
				$('.galeria-prev, .galeria-next').removeAttr('data-galeria-custom');

				if(response.content.prev > 0){
					construct_item(response.content.prev, response.content.prev_src);
					$('.galeria-prev').attr('data-galeria-custom', response.content.prev).removeClass("disabled");
				}
				else{
					$('.galeria-prev').addClass("disabled");
				}

				if(response.content.next > 0){
					construct_item(response.content.next, response.content.next_src);
					$('.galeria-next').attr('data-galeria-custom', response.content.next).removeClass("disabled");
				}
				else{
					$('.galeria-next').addClass("disabled");
				}

				construct_comentario(response.comentarios);
			},
			error: function(response){
			}
		});

		setTimeout(function(){
			$('.galeria-modal').removeClass('lock');
		}, 350);
	}
}

function construct_item(id, src, active){

	var verify_foto = '#foto-'+id;

	if($(verify_foto).length == 0){

		var item = "";

			item += '<div id="foto-'+id+'" class="item">';
			item += '<div class="galeria-item">';
			item += '<img>';
			item += '</div></div>';

		$('.galeria-carousel').append(item);

		new_image(id, src);

		make_zoom(verify_foto, src);
	}
	
	if(active == true){

		$(".galeria-carousel .item").removeClass("active");

		$(verify_foto).addClass("active");
	}
}

function new_image(id, src){

	var verify_foto = $('#foto-'+id);

	verify_foto.find(".galeria-item").addClass('loading');

	img = new Image();

	img.onload = function(){		
		verify_foto.find(".galeria-item.loading").removeClass('loading');
	};

	img.src = src;
	verify_foto.find("img").attr("src", img.src);
}

function construct_comentario(comentarios){

	$('.galeria-modal-body').html(comentarios);

	resize_comment_list();
}

function construct_modal(hash, src){

	var modal = "";

		modal += '<div class="galeria-modal">';
		modal += 	'<div class="galeria-modal-image">';
		modal += 		'<div id="galeria-change" class="galeria-carousel"></div>';
		modal += 		'<div class="galeria-nav">';
		modal += 			'<button class="galeria-prev"><i class="fa fa-chevron-left"></i></button>';
		modal += 			'<button class="galeria-next"><i class="fa fa-chevron-right"></i></button>';
		modal +=		'</div>'
		modal += 		'<div class="galeria-control">';
		modal += 			'<button class="galeria-play"><i class="fa fa-play"></i></button>';
		modal += 			'<button class="galeria-pause"><i class="fa fa-pause"></i></button>';
		modal += 		'</div>';
		modal += 	'</div>';
		modal += 	'<div class="galeria-modal-body"></div>';
		modal += 	'<span class="galeria-modal-close">';
		modal += 		'<i class="fa fa-times"></i>';
		modal += 	'</span>';
		modal += '</div>';

	$('body').append(modal);

	var touch;
	var touch_start;
	var cont_touch = 0;	
	var myElement = document.getElementById('galeria-change');

	swipedetect(myElement, function(swipedir){
	    if (swipedir =='left')
	        transition_arrow_click("next");
	    if (swipedir =='right')
	        transition_arrow_click("prev");
	})
}

function make_zoom(target, src){
	// 768
	if( $(window).width() > 768){
		$(target).not(".load_zoom").zoom({url: src, on:'click'}).addClass("load_zoom");
	}
}