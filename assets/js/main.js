//RETORN URL BASE JQUERY
function base_url(caminho) {
    if (caminho == undefined) {
        caminho = '';
    }
    var url_base = $('meta[name="base_url"]').attr("content");
    return url_base + caminho;
}

//ALERT - MENSAGENS
function message(action, message) {
    var element = $('#alertMessage');

    element.removeClass('alert-success').removeClass('alert-danger').addClass('alert-' + action);
    element.find('p').html(message);
    
    element.slideDown(750);
    setTimeout(function(){
        element.slideUp(750);
    }, 4500);
}

$(window).load(function() {

    // fecha mensagens ao clicar
    $('#alertMessage').click(function(e) {
        $(this).slideUp(750);
    });

    if ($('body').hasClass('loading')) {
        $('body').addClass('loaded').removeClass('loading');
    }
    // Ratio Img
    $('.ratio-img img').each(function(i) {
        var imgW = this.width,
            imgH = this.height,
            divW = $(this).parent('.ratio-img').width(),
            divH = $(this).parent('.ratio-img').height();

        var imgClass = (imgW / imgH < divW / divH ? 'tall' : 'wide');
        $(this).addClass(imgClass);

    });
});

$(document).ready(function() {

    $('body').addClass('loading');

    // Parallax
    function startParallax() {
        var parallaxElements = $('.parallax'),
            parallaxQuantity = parallaxElements.length;
        window.requestAnimationFrame(function() {
            for (var i = 0; i < parallaxQuantity; i++) {
                var currentElement = parallaxElements.eq(i);
                var translateX = ($(currentElement).hasClass('center-x') ? '-50%' : '0');
                var parallaxRatio = currentElement.data('parallax');
                var parallaxOpac = currentElement.data('parallax-opac');
                var scrolled = $(window).scrollTop();
                var screenStart = currentElement.offset().top - $(window).height();
                var screenEnd = currentElement.offset().top + currentElement.height() + $(window).height();

                if (scrolled > screenStart && scrolled < screenEnd) {
                    currentElement.css({
                        'transform': 'translate3d(' + translateX + ',' + scrolled * parallaxRatio + 'px, 0)'
                    });
                    if (typeof parallaxOpac !== typeof undefined && parallaxOpac !== false) {
                        var opac = 1 - ((scrolled / parallaxRatio) / (currentElement.offset().top + currentElement.height()) * -1); // precisa de melhoria

                        opac = (opac < 0 ? 0 : opac)
                        opac = (opac > 1 ? 1 : opac)

                        currentElement.css({
                            'opacity': opac
                        });
                    }
                }
            }
        });
    }

    startParallax();
    $(window).on('scroll', function() {
        startParallax();
    });

    $('#topo').sticky();

    if ($(window).width() > 992) {
        $('.fixed').stick_in_parent({
            offset_top: 130
        });
    }

    function verifyEdges(elem) {
        var totalW = 0;
        $(elem).find('.filters-item').each(function(index, el) {
            totalW += $(el).outerWidth();
        });

        //console.log($.type(elem.scrollLeft));
        if ($.type(elem.scrollLeft) !== 'number') {
            elem.scrollLeft = 0;
        }

        if (elem.scrollLeft == 0) {
            $(elem).parent().addClass('left');
        } else {
            $(elem).parent().removeClass('left');
        }
        if (totalW < $(elem).outerWidth() || elem.scrollLeft + $(elem).outerWidth() >= totalW) {
            $(elem).parent().addClass('right');
        } else {
            $(elem).parent().removeClass('right');
        }

    }

    $('.filters-content').on('mousewheel mouseup mousemove', function(e, delta) {
        if (e.type === 'mousewheel') {
            this.scrollLeft -= (delta * 20);
        }

        verifyEdges(this);

        if (e.type === 'mousewheel') {
            e.preventDefault();
        }
    });

    verifyEdges($('.filters-content'));

    // share buttons
    $('body').on('click', '.btn-share', function(e) {

        var href = $(this).data('href');
        href = href.replace('{{ url }}', window.location.href);

        var width = '750';
        var height = '500';
        var left = (screen.width / 2) - (width / 2);
        var top = (screen.height / 2) - (height / 2);

        window.open(href, '', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);

    });

});

// GERENCIA COOKIES

function mySetCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function myGetCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return false
}

function myDelCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

