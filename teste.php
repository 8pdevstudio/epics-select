<!DOCTYPE html>
<html lang="pt_BR"><head>

	<title>EPICS Select - Epics 10 anos</title>

    <meta charset="UTF-8">

    <link rel="shortcut icon" href="https://www.epics.io/assets/images/favicon.png">

    <meta name="description" content="">
    <meta name="keywords" content="seleção de fotos, aprovação de fotos, aprovação de álbuns, epics select">

    <meta name="language" content="pt-br">
    <meta name="distribution" content="global">
    <meta name="robots" content="noindex, nofollow">
    <meta name="rating" content="general">
    <meta name="googlebot" content="1. yes">
    <meta name="robots" content="noarchive">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="base_url" content="https://www.epics.io/">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#333333">
    <meta name="msapplication-navbutton-color" content="#333333">
    <meta name="apple-mobile-web-app-status-bar-style" content="#333333">
    
    <link rel="stylesheet" href="https://www.epics.io/assets/build/public.min.css">
    <link rel="stylesheet" href="https://www.epics.io/assets/fonts/icons/font-awesome.min.css">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:image" content="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7841-ts1573162042.jpg">
    <meta property="og:image:type" content="image/jpeg">

    <script src="https://connect.facebook.net/pt_BR/sdk.js?hash=7702d293fd4e03c088cd2c1bf1d754b4&amp;ua=modern_es6" async="" crossorigin="anonymous"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script id="facebook-jssdk" src="https://connect.facebook.net/pt_BR/sdk.js"></script><script>
        //MANTER O USADO NO LOGIN POR ÚLTIMO
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/pt_BR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        window.fbAsyncInit = function() {
            FB.init({
                appId       : '978856318892391',
                cookie      : true,
                xfbml       : true,
                version     : 'v4.0'
            });
            FB.AppEvents.logPageView();
        };
    </script>

            <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-150363507-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-150363507-1');
        </script>

        <!-- Hotjar Tracking Code for https://www.epics.io -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1534666,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script><script async="" src="https://static.hotjar.com/c/hotjar-1534666.js?sv=6"></script>
    
<style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_dialog_advanced{border-radius:8px;padding:10px}.fb_dialog_content{background:#fff;color:#373737}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{left:5px;right:auto;top:5px}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{height:100%;left:0;margin:0;overflow:visible;position:absolute;top:-10000px;transform:none;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{background:none;height:auto;min-height:initial;min-width:initial;width:auto}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{clear:both;color:#fff;display:block;font-size:18px;padding-top:20px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .4);bottom:0;left:0;min-height:100%;position:absolute;right:0;top:0;width:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_mobile .fb_dialog_iframe{position:sticky;top:0}.fb_dialog_content .dialog_header{background:linear-gradient(from(#738aba), to(#2c4987));border-bottom:1px solid;border-color:#043b87;box-shadow:white 0 1px 1px -1px inset;color:#fff;font:bold 14px Helvetica, sans-serif;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:linear-gradient(from(#4267B2), to(#2a4887));background-clip:padding-box;border:1px solid #29487d;border-radius:3px;display:inline-block;line-height:18px;margin-top:3px;max-width:85px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{background:none;border:none;color:#fff;font:bold 12px Helvetica, sans-serif;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #4a4a4a;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f5f6f7;border:1px solid #4a4a4a;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-position:50% 50%;background-repeat:no-repeat;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}</style><script async="" src="https://script.hotjar.com/modules.ad1e941217c27a6ed973.js" charset="utf-8"></script><style type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style><style type="text/css">
button.btn[data-v-cef78672]{display:inline-block;font-weight:300;line-height:1.25;text-align:center;white-space:nowrap;vertical-align:middle;user-select:none;border:1px solid transparent;cursor:pointer;letter-spacing:1px;transition:all .15s ease
}
button.btn.btn-sm[data-v-cef78672]{padding:.4rem .8rem;font-size:.8rem;border-radius:.2rem
}
button.btn.btn-primary[data-v-cef78672]{color:#fff;background-color:#45C8F1;border-color:#45C8F1
}
button.btn.btn-outline-primary[data-v-cef78672]{color:#45C8F1;background-color:transparent;border-color:#45C8F1
}
button.btn.btn-danger[data-v-cef78672]{color:#fff;background-color:#FF4949;border-color:#FF4949
}
.text-muted[data-v-cef78672]{color:#8492A6
}
.text-center[data-v-cef78672]{text-align:center
}
.drop-down-enter[data-v-cef78672],.drop-down-leave-to[data-v-cef78672]{transform:translateX(0) translateY(-20px);transition-timing-function:cubic-bezier(0.74, 0.04, 0.26, 1.05);opacity:0
}
.drop-down-enter-active[data-v-cef78672],.drop-down-leave-active[data-v-cef78672]{transition:all .15s
}
.move-left-enter[data-v-cef78672],.move-left-leave-to[data-v-cef78672]{transform:translateY(0) translateX(-80px);transition-timing-function:cubic-bezier(0.74, 0.04, 0.26, 1.05);opacity:0
}
.move-left-enter-active[data-v-cef78672],.move-left-leave-active[data-v-cef78672]{transition:all .15s
}
.shake[data-v-cef78672]{animation:shake-data-v-cef78672 0.82s cubic-bezier(0.36, 0.07, 0.19, 0.97) both;transform:translate3d(0, 0, 0)
}
@keyframes shake-data-v-cef78672{
10%,90%{transform:translate3d(-1px, 0, 0)
}
20%,80%{transform:translate3d(2px, 0, 0)
}
30%,50%,70%{transform:translate3d(-4px, 0, 0)
}
40%,60%{transform:translate3d(4px, 0, 0)
}
}
.pulse[data-v-cef78672]{animation:pulse-data-v-cef78672 2s ease infinite
}
@keyframes pulse-data-v-cef78672{
0%{opacity:.7
}
50%{opacity:.4
}
100%{opacity:.7
}
}
.flash-once[data-v-cef78672]{animation:flash-once 3.5s ease 1
}
@keyframes fade-up-data-v-cef78672{
0%{transform:translate3d(0, 10px, 0);opacity:0
}
100%{transform:translate3d(0, 0, 0);opacity:1
}
}
.fade-in[data-v-cef78672]{animation:fade-in-data-v-cef78672 .3s ease-in-out
}
@keyframes fade-in-data-v-cef78672{
0%{opacity:0
}
100%{opacity:1
}
}
.spin[data-v-cef78672]{animation-name:spin-data-v-cef78672;animation-duration:2000ms;animation-iteration-count:infinite;animation-timing-function:linear
}
@keyframes spin-data-v-cef78672{
from{transform:rotate(0deg)
}
to{transform:rotate(360deg)
}
}
.bounceIn[data-v-cef78672]{animation-name:bounceIn-data-v-cef78672;transform-origin:center bottom;animation-duration:1s;animation-fill-mode:both;animation-iteration-count:1
}
@keyframes bounceIn-data-v-cef78672{
0%,20%,40%,60%,80%,100%{-webkit-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)
}
0%{opacity:1;-webkit-transform:scale3d(0.8, 0.8, 0.8);transform:scale3d(0.8, 0.8, 0.8)
}
20%{-webkit-transform:scale3d(1.1, 1.1, 1.1);transform:scale3d(1.1, 1.1, 1.1)
}
40%{-webkit-transform:scale3d(0.9, 0.9, 0.9);transform:scale3d(0.9, 0.9, 0.9)
}
60%{opacity:1;-webkit-transform:scale3d(1.03, 1.03, 1.03);transform:scale3d(1.03, 1.03, 1.03)
}
80%{-webkit-transform:scale3d(0.97, 0.97, 0.97);transform:scale3d(0.97, 0.97, 0.97)
}
100%{opacity:1;-webkit-transform:scale3d(1, 1, 1);transform:scale3d(1, 1, 1)
}
}
@keyframes dots-data-v-cef78672{
0%,20%{color:rgba(0,0,0,0);text-shadow:0.25em 0 0 rgba(0,0,0,0),0.5em 0 0 rgba(0,0,0,0)
}
40%{color:#8492A6;text-shadow:0.25em 0 0 rgba(0,0,0,0),0.5em 0 0 rgba(0,0,0,0)
}
60%{text-shadow:0.25em 0 0 #8492A6,0.5em 0 0 rgba(0,0,0,0)
}
80%,100%{text-shadow:.25em 0 0 #8492A6, .5em 0 0 #8492A6
}
}
@keyframes recording-data-v-cef78672{
0%{box-shadow:0px 0px 5px 0px rgba(173,0,0,0.3)
}
65%{box-shadow:0px 0px 5px 5px rgba(173,0,0,0.3)
}
90%{box-shadow:0px 0px 5px 5px rgba(173,0,0,0)
}
}
html[data-v-cef78672]{max-height:500px
}
body[data-v-cef78672]{margin:0;font-size:100%;color:#3C4858;width:350px;max-height:500px
}
a[data-v-cef78672]{text-decoration:none;color:#45C8F1
}
h1[data-v-cef78672],h2[data-v-cef78672],h3[data-v-cef78672],h4[data-v-cef78672]{margin-top:0
}
.container_selected_area[data-v-cef78672]{padding:0px;position:fixed;z-index:1000000;top:0px;left:0px;right:0px;bottom:0px;display:none;visibility:hidden
}
.container_selected_area.active[data-v-cef78672]{visibility:visible;display:block
}
.container_selected_area .label[data-v-cef78672]{font-family:"Didact Gothic Regular",sans-serif;font-size:22px;text-align:center;padding-top:15px
}
.container_selected_area .area[data-v-cef78672]{border:1px solid #1e83ff;background:rgba(30,131,255,0.1);display:none;position:fixed;z-index:1000000;box-sizing:border-box
}
.container_selected_area .area.active[data-v-cef78672]{display:block;top:0px;left:0px
}
.hide[data-v-cef78672]{display:none
}
</style></head>
<body class="loaded" style="">

	<section id="banner" style="background-image: url(https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7841-ts1573162042.jpg)">
	<div class="container h-100">
		<div class="content text-center text-white h-100 d-flex flex-column justify-content-between align-items-center col-md-8 offset-md-2">
							<!-- Verifica se é a galeria do Evandro para demonstrativo do Beta (Burning Man 2019) -->
				<a href="http://www.evandrorocha.com.br" target="_blank">
											<img src="https://e9ea5e9783bfb654701a-ce36c02b72d67152a15845c2524ecd70.ssl.cf1.rackcdn.com/Logo_thumb.png" alt="Evandro Rocha" title="Evandro Rocha">
									</a>
						<div class="pt-5">
				<h1 class="title title-1 mb-0">Epics 10 anos</h1>
				<small>
					22.05.2019 
				</small>
			</div>
		</div>
	</div>
</section>

<div class="body-content">
	<div id="topo-sticky-wrapper" class="sticky-wrapper is-sticky" style="height: 104px;"><header id="topo" style="width: 1903px; position: fixed; top: 0px;">
		<div class="container">
			<div class="row mx-0 py-3 align-items-center justify-content-between">
				
				<div class="col-auto d-none d-md-block">
					<h2 class="title title-2 mb-1">Epics 10 anos</h2>
					<span class="text-muted">Fotos por: Evandro Rocha</span>
				</div>

				<div class="col text-right">

					<span class="d-inline-block mr-1 mr-md-3">
						<!-- (no theme) || .theme-1 || .theme-2 -->
														<div class="custom-control custom-checkbox number px-1 px-md-2 theme-1">
									<label class="cursor-pointer">
								  		<input type="checkbox" class="custom-control-input" name="filter[selection][0]" value="105">
								 		<p id="count-selection-105" class="custom-control-label m-0 mt-1 p-0">
								  			3		
								  		</p>
								  		<span class="custom-control-text m-0 p-0">
								  			Seleção		
								  		</span>
								  	</label>
								</div>
							
					</span>

					<button class="btn btn-square btn-transparent circle ml-4 modal-ajax btn-finalizar-carrinho" data-href="https://www.epics.io/galleries/finish_get/0a10aef8355f07e41efdd2c8cedee483">
						<i class="far fa-shopping-cart"></i>
						<span class="badge badge-pill badge-dark cart-quantity" style="position: absolute; font-size: 10px; padding: 4px 6px; border-radius: 50%; top: 3px; left: 25px;">
							0						</span>
					</button>

					
					<button class="btn btn-square btn-transparent circle ml-1 d-none d-md-inline-block" data-toggle="modal" data-target="#modal-search">
						<i class="fa fa-search"></i>
					</button>

											<button class="btn btn-lg btn-primary my-0 mr-md-3 modal-ajax btn-finalizar-carrinho" data-href="https://www.epics.io/galleries/finish_get/0a10aef8355f07e41efdd2c8cedee483">
							Finalizar						</button>
					
					<div class="dropdown d-inline-block">
						<button class="ratio-img ratio-img-avatar ratio-img-avatar-3 d-none d-md-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<span>RC</span>
													</button>
						<!-- Tirar d-none quando configurar dropdown -->
						<button class="btn btn-square btn-transparent d-md-none d-none m-0" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
						<!-- Tirar d-none quando configurar dropdown -->
						<div class="d-none dropdown-menu dropdown-menu-right menu-clickable">
							<div class="d-flex mx-n3 align-items-center">
								<div class="col-auto">
									<div class="ratio-img ratio-img-avatar ratio-img-avatar-3">
																					<span>RC</span>
																			</div>
								</div>
								<div class="col pl-0 ff-title">
									<strong class="d-block">
										RAFAEL NASCIMENTO DE CARVALHO									</strong>
									<span class="text-muted">
										rafael@8pdev.studio									</span>
								</div>
							</div>
							<div class="dropdown-divider"></div>
							<a href="https://www.epics.io/dashboard" class="dropdown-item">
								Minhas galerias							</a>
							<a href="https://www.epics.com.br/accounts/login/meu_cadastro" target="_blank" class="dropdown-item">
								Configuração de conta							</a>
							<a href="https://www.epics.com.br/accounts/login/meus_pedidos" target="_blank" class="dropdown-item">
								Compras							</a>
							<a href="https://suporte.epics.com.br/hc/pt-br" target="_blank" class="dropdown-item">
								Ajuda							</a>
							<a href="https://www.epics.io/accounts/logout" class="dropdown-item dropdown-item-danger">
								Sair							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header></div>
	<div id="filtros" class="pb-3 pt-0 pt-md-3">
		<div class="container px-0 px-md-3">
			<span class="row mx-0 justify-content-between">
				<div class="col-md-6 pr-lg-5 pl-0 pr-0 pr-md-3">
					<div class="filters-list left right">
						<div class="filters-content dragscroll">

							<div class="filters-item px-2">
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" class="custom-control-input" id="filter-type-1" name="filter[type]" value="1">
								  	<label class="btn custom-control-label" for="filter-type-1">
								  		Selecionadas								  	</label>
								</div>
							</div>

							<div class="filters-item px-2">
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" class="custom-control-input" id="filter-order-by-1" name="filter[order_by]" value="1">
								  	<label class="btn custom-control-label" for="filter-order-by-1">
								  		<i class="fa fa-heart text-danger mr-2"></i> Mais curtidas								  	</label>
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="col-md-6 pl-lg-5 pr-0 d-none d-md-block">
					<div class="filters-list text-right left right">
						<div class="filters-content dragscroll">

							<!--
							<div class="filters-item px-2">
								<div class="custom-control custom-checkbox transparent">
								  <input type="checkbox" class="custom-control-input" id="filtro-making-of" name="filtros">
								  <label class="btn custom-control-label" for="filtro-making-of">making of</label>
								</div>
							</div>
							-->

						</div>
					</div>
				</div>

			</span>
		</div>
	</div>

	<div class="m-2 mb-5">
				<div id="grid-main" class="flex-images row fotos-grid mx-0 mx-lg-n1">

	<div class="flex-images row fotos-grid mx-0 mx-lg-n1">

		
			<div class="item-foto item-foto-308118756 dropdown  active" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6822.jpg?id=308118756" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118756" title="Foto 308118756" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6822_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118756" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" checked="" data-id="308118756" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118756">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118756" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118756">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6822.jpg?id=308118756" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118771 dropdown  active" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6827-ts1573162042.jpg?id=308118771" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118771" title="Foto 308118771" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6827-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118771" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" checked="" data-id="308118771" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118771">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118771" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118771">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6827-ts1573162042.jpg?id=308118771" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118796 dropdown  active" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6853-ts1573162042.jpg?id=308118796" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118796" title="Foto 308118796" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6853-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118796" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" checked="" data-id="308118796" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118796">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118796" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118796">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6853-ts1573162042.jpg?id=308118796" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118813 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6856-ts1573162042.jpg?id=308118813" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118813" title="Foto 308118813" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6856-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118813" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118813" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118813">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118813" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118813">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6856-ts1573162042.jpg?id=308118813" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118829 dropdown" data-w="460" data-h="307" style="width: 479px; height: 320px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6860-ts1573162042.jpg?id=308118829" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118829" title="Foto 308118829" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6860-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118829" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118829" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118829">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118829" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118829">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6860-ts1573162042.jpg?id=308118829" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118846 dropdown" data-w="307" data-h="460" style="width: 214px; height: 320px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6864-ts1573162042.jpg?id=308118846" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118846" title="Foto 308118846" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6864-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118846" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118846" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118846">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118846" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118846">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6864-ts1573162042.jpg?id=308118846" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118866 dropdown" data-w="307" data-h="460" style="width: 214px; height: 320px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6868-ts1573162042.jpg?id=308118866" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118866" title="Foto 308118866" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6868-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118866" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118866" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118866">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118866" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118866">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6868-ts1573162042.jpg?id=308118866" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118885 dropdown" data-w="460" data-h="307" style="width: 479px; height: 320px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6871-ts1573162042.jpg?id=308118885" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118885" title="Foto 308118885" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6871-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118885" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118885" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118885">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118885" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118885">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6871-ts1573162042.jpg?id=308118885" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118903 dropdown" data-w="460" data-h="307" style="width: 475px; height: 320px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6876-ts1573162042.jpg?id=308118903" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118903" title="Foto 308118903" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6876-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118903" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118903" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118903">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118903" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118903">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6876-ts1573162042.jpg?id=308118903" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118918 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6879-ts1573162042.jpg?id=308118918" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118918" title="Foto 308118918" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6879-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118918" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118918" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118918">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118918" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118918">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6879-ts1573162042.jpg?id=308118918" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118934 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6885-ts1573162042.jpg?id=308118934" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118934" title="Foto 308118934" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6885-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118934" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118934" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118934">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118934" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118934">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6885-ts1573162042.jpg?id=308118934" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118955 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6889-ts1573162042.jpg?id=308118955" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118955" title="Foto 308118955" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6889-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118955" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118955" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118955">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118955" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118955">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6889-ts1573162042.jpg?id=308118955" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118968 dropdown" data-w="307" data-h="460" style="width: 187px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6893-ts1573162042.jpg?id=308118968" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118968" title="Foto 308118968" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6893-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118968" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118968" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118968">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118968" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118968">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6893-ts1573162042.jpg?id=308118968" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308118984 dropdown" data-w="460" data-h="307" style="width: 417px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6897-ts1573162042.jpg?id=308118984" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308118984" title="Foto 308118984" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6897-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308118984" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308118984" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308118984">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308118984" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308118984">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6897-ts1573162042.jpg?id=308118984" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119005 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6901-ts1573162042.jpg?id=308119005" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119005" title="Foto 308119005" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6901-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119005" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119005" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119005">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119005" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119005">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6901-ts1573162042.jpg?id=308119005" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119020 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6905-ts1573162042.jpg?id=308119020" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119020" title="Foto 308119020" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6905-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119020" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119020" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119020">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119020" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119020">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6905-ts1573162042.jpg?id=308119020" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119041 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6907-ts1573162042.jpg?id=308119041" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119041" title="Foto 308119041" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6907-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119041" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119041" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119041">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119041" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119041">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6907-ts1573162042.jpg?id=308119041" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119060 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6912-ts1573162042.jpg?id=308119060" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119060" title="Foto 308119060" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6912-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119060" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119060" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119060">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119060" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119060">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6912-ts1573162042.jpg?id=308119060" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119075 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6915-ts1573162042.jpg?id=308119075" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119075" title="Foto 308119075" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6915-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119075" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119075" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119075">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119075" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119075">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6915-ts1573162042.jpg?id=308119075" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119100 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6918-ts1573162042.jpg?id=308119100" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119100" title="Foto 308119100" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6918-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119100" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119100" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119100">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119100" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119100">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6918-ts1573162042.jpg?id=308119100" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119123 dropdown" data-w="307" data-h="460" style="width: 187px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6924-ts1573162042.jpg?id=308119123" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119123" title="Foto 308119123" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6924-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119123" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119123" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119123">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119123" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119123">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6924-ts1573162042.jpg?id=308119123" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119139 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6927-ts1573162042.jpg?id=308119139" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119139" title="Foto 308119139" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6927-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119139" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119139" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119139">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119139" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119139">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6927-ts1573162042.jpg?id=308119139" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119152 dropdown" data-w="460" data-h="307" style="width: 417px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6934-ts1573162042.jpg?id=308119152" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119152" title="Foto 308119152" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6934-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119152" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119152" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119152">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119152" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119152">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6934-ts1573162042.jpg?id=308119152" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119175 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6941-ts1573162042.jpg?id=308119175" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119175" title="Foto 308119175" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6941-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119175" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119175" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119175">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119175" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119175">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6941-ts1573162042.jpg?id=308119175" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119198 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6946-ts1573162042.jpg?id=308119198" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119198" title="Foto 308119198" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6946-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119198" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119198" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119198">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119198" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119198">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6946-ts1573162042.jpg?id=308119198" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119218 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6950-ts1573162042.jpg?id=308119218" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119218" title="Foto 308119218" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6950-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119218" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119218" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119218">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119218" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119218">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6950-ts1573162042.jpg?id=308119218" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119237 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6951-ts1573162042.jpg?id=308119237" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119237" title="Foto 308119237" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6951-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119237" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119237" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119237">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119237" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119237">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6951-ts1573162042.jpg?id=308119237" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119256 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6955-ts1573162042.jpg?id=308119256" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119256" title="Foto 308119256" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6955-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119256" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119256" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119256">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119256" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119256">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6955-ts1573162042.jpg?id=308119256" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119276 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6957-ts1573162042.jpg?id=308119276" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119276" title="Foto 308119276" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6957-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119276" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119276" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119276">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119276" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119276">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6957-ts1573162042.jpg?id=308119276" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119298 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6961-ts1573162042.jpg?id=308119298" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119298" title="Foto 308119298" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6961-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119298" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119298" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119298">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119298" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119298">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6961-ts1573162042.jpg?id=308119298" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119316 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6965-ts1573162042.jpg?id=308119316" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119316" title="Foto 308119316" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6965-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119316" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119316" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119316">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119316" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119316">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6965-ts1573162042.jpg?id=308119316" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119338 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6967-ts1573162042.jpg?id=308119338" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119338" title="Foto 308119338" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6967-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119338" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119338" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119338">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119338" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119338">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6967-ts1573162042.jpg?id=308119338" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119359 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6973-ts1573162042.jpg?id=308119359" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119359" title="Foto 308119359" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6973-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119359" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119359" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119359">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119359" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119359">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6973-ts1573162042.jpg?id=308119359" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119375 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6978-ts1573162042.jpg?id=308119375" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119375" title="Foto 308119375" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6978-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119375" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119375" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119375">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119375" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119375">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6978-ts1573162042.jpg?id=308119375" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119396 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6983-ts1573162042.jpg?id=308119396" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119396" title="Foto 308119396" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6983-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119396" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119396" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119396">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119396" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119396">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6983-ts1573162042.jpg?id=308119396" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119411 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6991-ts1573162042.jpg?id=308119411" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119411" title="Foto 308119411" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6991-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119411" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119411" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119411">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119411" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119411">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6991-ts1573162042.jpg?id=308119411" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119431 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6997-ts1573162042.jpg?id=308119431" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119431" title="Foto 308119431" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6997-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119431" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119431" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119431">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119431" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119431">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab6997-ts1573162042.jpg?id=308119431" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119448 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7005-ts1573162042.jpg?id=308119448" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119448" title="Foto 308119448" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7005-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119448" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119448" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119448">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119448" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119448">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7005-ts1573162042.jpg?id=308119448" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119467 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7009-ts1573162042.jpg?id=308119467" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119467" title="Foto 308119467" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7009-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119467" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119467" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119467">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119467" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119467">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7009-ts1573162042.jpg?id=308119467" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119491 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7010-ts1573162042.jpg?id=308119491" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119491" title="Foto 308119491" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7010-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119491" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119491" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119491">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119491" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119491">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7010-ts1573162042.jpg?id=308119491" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119506 dropdown" data-w="307" data-h="460" style="width: 187px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7015-ts1573162042.jpg?id=308119506" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119506" title="Foto 308119506" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7015-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119506" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119506" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119506">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119506" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119506">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7015-ts1573162042.jpg?id=308119506" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119525 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7017-ts1573162042.jpg?id=308119525" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119525" title="Foto 308119525" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7017-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119525" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119525" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119525">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119525" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119525">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7017-ts1573162042.jpg?id=308119525" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119540 dropdown" data-w="460" data-h="307" style="width: 419px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7022-ts1573162042.jpg?id=308119540" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119540" title="Foto 308119540" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7022-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119540" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119540" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119540">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119540" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119540">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7022-ts1573162042.jpg?id=308119540" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119562 dropdown" data-w="460" data-h="307" style="width: 417px; height: 280px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7027-ts1573162042.jpg?id=308119562" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119562" title="Foto 308119562" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7027-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119562" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119562" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119562">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119562" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119562">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7027-ts1573162042.jpg?id=308119562" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119581 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7031-ts1573162042.jpg?id=308119581" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119581" title="Foto 308119581" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7031-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119581" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119581" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119581">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119581" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119581">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7031-ts1573162042.jpg?id=308119581" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119597 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7037-ts1573162042.jpg?id=308119597" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119597" title="Foto 308119597" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7037-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119597" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119597" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119597">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119597" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119597">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7037-ts1573162042.jpg?id=308119597" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119620 dropdown" data-w="460" data-h="307" style="width: 468px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7041-ts1573162042.jpg?id=308119620" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119620" title="Foto 308119620" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7041-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119620" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119620" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119620">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119620" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119620">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7041-ts1573162042.jpg?id=308119620" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119636 dropdown" data-w="460" data-h="307" style="width: 465px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7044-ts1573162042.jpg?id=308119636" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119636" title="Foto 308119636" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7044-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119636" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119636" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119636">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119636" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119636">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7044-ts1573162042.jpg?id=308119636" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119651 dropdown" data-w="460" data-h="307" style="width: 467px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7050-ts1573162042.jpg?id=308119651" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119651" title="Foto 308119651" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7050-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119651" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119651" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119651">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119651" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119651">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7050-ts1573162042.jpg?id=308119651" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
			<div class="item-foto item-foto-308119668 dropdown" data-w="460" data-h="307" style="width: 467px; height: 312px; display: block;">

				<a data-href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7055-ts1573162042.jpg?id=308119668" data-fancybox="galeria" title="Ver foto">
					<div class="img" alt="Foto 308119668" title="Foto 308119668" style="background-image: url('https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7055-ts1573162042_thumb.jpg')">
						<img src="https://www.epics.io/assets/images/img_2498.png" title="Foto 308119668" class="protected">
					</div>
				</a>

				<div class="top-left d-flex flex-column align-items-start text-white ">

												<div class="custom-control custom-checkbox number number-sm label-right px-2 theme-1">
								<label class="m-0 cursor-pointer">
							  		<input type="checkbox" name="ac_selection" class="custom-control-input" data-id="308119668" data-selection="105">
							  		<p class="custom-control-label m-0 mr-2 p-0">
							  			<i class="fal fa-check"></i>
							  		</p>
							  		<span class="custom-control-text m-0 p-0">Seleção</span>
							  	</label>
							</div>
						

											<button type="button" class="btn btn-sm btn-transparent my-2 cart-btn" data-id="308119668">
							<i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos						</button>
					
				</div>

				<div class="top-right text-white">
					<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						
													<a class="dropdown-item ac_hidden cursor-pointer" data-id="308119668" data-action="insert">
								Ocultar
							</a>
						
					</div>
				</div>

				<div class="bottom-right text-white">

										<a class="btn btn-square btn-transparent mb-0 cursor-pointer ac_like " data-id="308119668">
						<i class="far fa-heart"></i>
					</a>

					<!-- <a href="https://d2900ab399f2cb3347de-c102896f121be6615b3d16101b6f31cc.ssl.cf1.rackcdn.com/fab7055-ts1573162042.jpg?id=308119668" class="btn btn-square btn-transparent ml-1" data-fancybox="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a> -->
				</div>

			</div>
		
	</div>

	<div class="container mt-4">
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-end"><li class="page-item active"><span class="page-link">1</span></li><li class="page-item"><span class="page-link"><a href="https://www.epics.io/galleries/view_photos_post/50" data-ci-pagination-page="2">2</a></span></li><li class="page-item"><span class="page-link"><a href="https://www.epics.io/galleries/view_photos_post/100" data-ci-pagination-page="3">3</a></span></li><li class="page-item"><span class="page-link"><a href="https://www.epics.io/galleries/view_photos_post/150" data-ci-pagination-page="4">4</a></span></li><li class="page-item"><span class="page-link"><a href="https://www.epics.io/galleries/view_photos_post/200" data-ci-pagination-page="5">5</a></span></li><li class="page-item"><span class="page-link" aria-label="Next"><a href="https://www.epics.io/galleries/view_photos_post/50" data-ci-pagination-page="2" rel="next"><span aria-hidden="true"><i class="far fa-chevron-right"></i></span></a></span></li></ul>		</nav>
	</div>

</div>
	</div>
</div>

<footer id="rodape" class="pt-5">
	<div class="container">
		<div class="row align-items-center pb-5">
			<div class="col-md-4 mb-5 mb-md-0 text-center text-md-left">
				<div class="ff-title mb-4">
					<strong class="d-block mb-2">Epics 10 anos</strong>
					<small class="d-block text-muted">22.05.2019</small>
				</div>
				<div class="mb-4">
					<p>
						"Um casamento cheio de energia"
						<br>
						Aqui você poderá falar sobre o evento e dar uma moral para os fornecedores!
					</p>
				</div>
				<a href="#" target="_blank" class="row justify-content-center text-muted">
					</a><div class="col-auto col-md pl-0 text-left"><a href="#" target="_blank" class="row justify-content-center text-muted">
												</a><a href="https://www.google.com/maps/place/Fazenda+Quinzeiro+-+Minas+Gerais" target="_blank">
							<p>
								<i class="fa fa-map-marker-alt"></i>
								Fazenda Quinzeiro - Minas Gerais							</p>
						</a>
					</div>
				
			</div>
			<div class="col-md-4 mb-5 mb-md-0">
				<div class="row justify-content-center align-items-center text-center text-xl-left">
											<div class="col-auto mb-3 mb-xl-0">
							<!-- Verifica se é a galeria do Evandro para demonstrativo do Beta (Burning Man 2019) -->
							<a href="http://www.evandrorocha.com.br" target="_blank">
																	<img src="https://e9ea5e9783bfb654701a-ce36c02b72d67152a15845c2524ecd70.ssl.cf1.rackcdn.com/Logo_thumb.png" alt="Evandro Rocha" title="Evandro Rocha">
															</a>
						</div>
										<div class="col-12 col-xl-auto pl-0">
						<span>Fotos por:</span>
						<h3 class="title title-2 mt-0 mb-2">EVANDRO ROCHA</h3>
						<!-- TIRAR /LOGIN QUANDO FOR LANÇAR PARA TODOS, FOI INSERIDO APENAS PARA O BETA -->
						<a href="http://www.evandrorocha.com.br" target="_blank" class="text-muted">
							www.evandrorocha.com.br						</a>
						<!-- <div class="mt-2 mx-n2">
							<a href="#" target="_blank" class="text-muted p-2"><i class="fab fa-whatsapp fz-2x"></i></a>
							<a href="#" target="_blank" class="text-muted p-2"><i class="fab fa-instagram fz-2x"></i></a>
							<a href="#" target="_blank" class="text-muted p-2"><i class="fab fa-facebook fz-2x"></i></a>
						</div> -->
					</div>
				</div>
			</div>
			<div class="col-md-4 text-center">
				<h3 class="ff-title fz-2x">Gostou das fotos?</h3>
				<!-- <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#modal-orcamento" disabled>
					Peça um orçamento		
				</button> -->
				<a href="https://www.epics.com.br/" target="_blank"><button class="btn btn-lg btn-primary">
					Peça um orçamento		
				</button></a>
			</div>
		</div>
		<div class="row py-3 text-center">
			<div class="col">
				<a href="https://www.epics.com.br/" target="_blank">
					<strong>EPICS</strong> | Soluções online para fotógrafos e videomakers				</a>
			</div>
		</div>
	</div>
</footer>


<div class="modal fade modal-overlay bg-white" id="modal-search" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">

    	<button class="btn btn-square btn-danger close" data-dismiss="modal">
    		<i class="fal fa-times"></i>
    	</button>

        <div class="modal-content h-100 flex-row">
            <div class="modal-body container">

				<span>
					<div class="input-group input-group-icon">
						<i class="far fa-search"></i>
						<input type="text" name="filter[search]" class="form-control form-control-sm" placeholder="Pesquisar suas fotos">
					</div>
				</span>
				
				<ul class="row align-items-center mx-0 mt-5">
					<li class="lh-1"><i class="fal fa-fw fa-eye-slash fz-2x"></i></li>
					<li class="pl-3">
						<div class="custom-control custom-checkbox">
						  	<input type="checkbox" class="custom-control-input" id="filtro-hidden" name="filter[hidden]" value="0">
						  	<label class="btn custom-control-label" for="filtro-hidden">
						  		mostrar fotos ocultas						  	</label>
						</div>
					</li>
				</ul>

				<div class="row">
					<div class="col-12 text-right">

						<hr>

						<button class="btn btn-lg btn-primary" data-dismiss="modal">
							<i class="far fa-search"></i>
							Filtrar Fotos						</button>

					</div>
				</div>

				
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ajax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content flex-row align-items-center">

    		

        </div>
    </div>
</div>

	<div class="modal fade" id="modal-compatilhar" tabindex="2" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
	    		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body">
					<h4 class="dropdown-header">Compartilhar</h4>

					<div class="row">
						<div class="col-12 text-center">

							
							<button data-href="https://api.whatsapp.com/send?text=Confira a galeria de fotos Epics 10 anos - {{ url }}" class="btn-share btn-lg mb-2">
								<i class="fab fa-whatsapp"></i>
							</button>

							<button data-href="https://facebook.com/sharer/sharer.php?u={{ url }}" class="btn-share btn-lg mb-2">
								<i class="fab fa-facebook"></i>
							</button>

							<button data-href="https://www.pinterest.com/pin/create/button?url={{ url }}&amp;description=Epics+10+anos" class="btn-share btn-lg mb-2">
								<i class="fab fa-pinterest"></i>
							</button>

							<button data-href="https://twitter.com/intent/tweet?text=Confira a galeria de fotos Epics 10 anos - {{ url }}" class="btn-share btn-lg mb-2">
								<i class="fab fa-twitter"></i>
							</button>

						</div>
					</div>

					<!-- <div class="form-group">
						<input type="text" class="form-control" placeholder="Digite um nome ou e-mail">
					</div>
					<div class="row text-center lh-1">
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								Se tiver imagem lista aqui
								<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia">
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
					</div> -->

	            </div>
	        </div>
	    </div>
	</div>

<input type="hidden" name="gi" value="1054797">
<input type="hidden" name="gt" value="0a10aef8355f07e41efdd2c8cedee483">
<input type="hidden" name="gp" value="1">
<input type="hidden" name="gs" value="7">
<input type="hidden" name="hs" value="1" required="">
    <div class="modal fade" id="modal-ajax" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>


        <div id="alertMessage" class="alert alert-success" style="display: none;">
        <p></p>
    </div>

        <script type="text/javascript" src="https://www.epics.io/assets/build/public.min.js"></script><div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div><iframe name="fb_xdm_frame_https" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://staticxx.facebook.com/connect/xd_arbiter.php?version=44#channel=f322c701c017a1c&amp;origin=https%3A%2F%2Fwww.epics.io" style="border: none;"></iframe></div><div></div></div></div>

    <script type="text/javascript" src="https://www.epics.io/assets/build/scripts/galleries.js"></script><script type="text/javascript" src="https://www.epics.io/assets/build/scripts/cart.js"></script>
    

<iframe name="_hjRemoteVarsFrame" title="_hjRemoteVarsFrame" id="_hjRemoteVarsFrame" src="https://vars.hotjar.com/box-90f3a29ef7448451db5af955688970d7.html" style="display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;"></iframe><input type="file" id="" name="file" style="display: none;"><div data-v-cef78672="" class="container_selected_area" style="cursor: url(&quot;chrome-extension://ialiedlpfknneamnbemcgmaboleiccdd/assets/images/cursor-imagen.svg&quot;) 9 9, crosshair;"><div data-v-cef78672="" class="area"></div></div></body></html>