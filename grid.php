<div class="flex-images row fotos-grid mx-0 mx-lg-n1">
<?php
	
	include('imgs.php');

	foreach ($imgs as $key => $img){

		$url = $img['thumb'];
		$image = getimagesize($url);

		?>

		<div class="item-foto dropdown" data-w="<?php echo $image[0]; ?>" data-h="<?php echo $image[1]; ?>">
			<a  href="#" title="Ver foto" data-galeria-custom="<?php echo $img['id']; ?>" data-galeria-src="<?php echo $img['src']; ?>">
				<div class="img">
					<img src="<?php echo $url; ?>" alt="Foto <?php echo $key; ?>" title="Foto <?php echo $key; ?>">
				</div>
			</a>
			<div class="top-left d-flex flex-column align-items-start text-white">
				<div class="custom-control custom-checkbox number number-sm theme-1 label-right px-2">
				  <input type="checkbox" class="custom-control-input foto-<?php echo $key ?>-categoria-album" name="categorias">
				  <label class="custom-control-label" data-for=".foto-<?php echo $key ?>-categoria-album"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-<?php echo $key ?>-categoria-album">Seleção Álbum</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-2 label-right px-2">
				  <input type="checkbox" class="custom-control-input foto-<?php echo $key ?>-categoria-sogra" name="categorias">
				  <label class="custom-control-label" data-for=".foto-<?php echo $key ?>-categoria-sogra"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-<?php echo $key ?>-categoria-sogra">Seleção Sogra</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-white label-right px-2">
				  <input type="checkbox" class="custom-control-input foto-<?php echo $key ?>-categoria-foto-10x15" name="categorias">
				  <label class="custom-control-label" data-for=".foto-<?php echo $key ?>-categoria-foto-10x15"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-<?php echo $key ?>-categoria-foto-10x15">Foto 10x15cm</label>
				</div>
				<button class="btn btn-sm btn-transparent my-2" data-toggle="dropdown"><i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos</button>
				<div class="dropdown-menu menu-clickable">
					<h4 class="dropdown-header">Outros Produtos</h4>
					<strong class="d-block ff-title mb-2">Seleção</strong>
					<a href="#" class="dropdown-item active">Álbum</a>
					<a href="#" class="dropdown-item">Sogra</a>
					<div class="dropdown-divider"></div>
					<strong class="d-block ff-title mb-2">Prints</strong>
					<a href="#" class="dropdown-item active">Foto Digital</a>
					<a href="#" class="dropdown-item">Foto tamanho 10x25cm</a>
					<a href="#" class="dropdown-item">Foto tamanho 26x36cm</a>
					<a href="#" class="btn btn-block btn-outline-primary">Criar Seleção</a>
				</div>
			</div>
			<div class="top-right text-white">
				<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item">Comentar</a>
					<a href="#" class="dropdown-item">Compartilhar</a>
					<a href="#" class="dropdown-item">Curtir</a>
					<a href="#" class="dropdown-item">Copiar link</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-item-danger">Ocultar foto</a>
				</div>
			</div>
			<div class="bottom-right text-white">
				<!-- Exemplo de foto curtida
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Descurtir foto"><i class="fa fa-heart text-danger"></i></a>
				-->
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Curtir foto"><i class="far fa-heart"></i></a>
				<!-- <a href="#" class="btn btn-square btn-transparent ml-1" data-galeria-custom="<?php echo get_img_name($url); ?>" title="Ver foto">
					<i class="far fa-expand-arrows-alt"></i>
				</a> -->
			</div>
		</div>
		<?php
	}

?>
</div>

<?php

/* exemplo -----

<ul class="fotos-grid mx-0 mx-lg-n1 row">
	<li class="grid-item m-1">
		<div class="item-foto dropdown">
			<div class="img">
				<img src="images/foto-1.jpg" alt="Foto 1" title="Foto 1">
			</div>
			<div class="top-left d-flex flex-column align-items-start text-white">
				<div class="custom-control custom-checkbox number number-sm theme-1 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-1-categoria-album" name="categorias">
				  <label class="custom-control-label" data-for=".foto-1-categoria-album"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-1-categoria-album">Seleção Álbum</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-2 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-1-categoria-sogra" name="categorias">
				  <label class="custom-control-label" data-for=".foto-1-categoria-sogra"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-1-categoria-sogra">Seleção Sogra</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-white label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-1-categoria-foto-10x15" name="categorias">
				  <label class="custom-control-label" data-for=".foto-1-categoria-foto-10x15"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-1-categoria-foto-10x15">Foto 10x15cm</label>
				</div>
				<button class="btn btn-sm btn-transparent my-2" data-toggle="dropdown"><i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos</button>
				<div class="dropdown-menu menu-clickable">
					<h4 class="dropdown-header">Outros Produtos</h4>
					<strong class="d-block ff-title mb-2">Seleção</strong>
					<a href="#" class="dropdown-item active">Álbum</a>
					<a href="#" class="dropdown-item">Sogra</a>
					<div class="dropdown-divider"></div>
					<strong class="d-block ff-title mb-2">Prints</strong>
					<a href="#" class="dropdown-item active">Foto Digital</a>
					<a href="#" class="dropdown-item">Foto tamanho 10x25cm</a>
					<a href="#" class="dropdown-item">Foto tamanho 26x36cm</a>
					<a href="#" class="btn btn-block btn-outline-primary">Criar Seleção</a>
				</div>
			</div>
			<div class="top-right text-white">
				<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item">Comentar</a>
					<a href="#" class="dropdown-item">Compartilhar</a>
					<a href="#" class="dropdown-item">Curtir</a>
					<a href="#" class="dropdown-item">Copiar link</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-item-danger">Ocultar foto</a>
				</div>
			</div>
			<div class="bottom-right text-white">
				<!-- Exemplo de foto curtida
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Descurtir foto"><i class="fa fa-heart text-danger"></i></a>
				-->
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Curtir foto"><i class="far fa-heart"></i></a>
				<a href="images/foto-grande.jpg" class="btn btn-square btn-transparent ml-1" data-galeria-custom="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a>
			</div>
		</div>
	</li>
	<li class="grid-item m-1">
		<div class="item-foto dropdown">
			<div class="img">
				<img src="images/foto-2.jpg" alt="Foto 2" title="Foto 2">
			</div>
			<div class="top-left d-flex flex-column align-items-start text-white">
				<div class="custom-control custom-checkbox number number-sm theme-1 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-2-categoria-album" name="categorias">
				  <label class="custom-control-label" data-for=".foto-2-categoria-album"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-2-categoria-album">Seleção Álbum</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-2 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-2-categoria-sogra" name="categorias">
				  <label class="custom-control-label" data-for=".foto-2-categoria-sogra"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-2-categoria-sogra">Seleção Sogra</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-white label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-2-categoria-foto-10x15" name="categorias">
				  <label class="custom-control-label" data-for=".foto-2-categoria-foto-10x15"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-2-categoria-foto-10x15">Foto 10x15cm</label>
				</div>
				<button class="btn btn-sm btn-transparent my-2" data-toggle="dropdown"><i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos</button>
				<div class="dropdown-menu menu-clickable">
					<h4 class="dropdown-header">Outros Produtos</h4>
					<strong class="d-block ff-title mb-2">Seleção</strong>
					<a href="#" class="dropdown-item active">Álbum</a>
					<a href="#" class="dropdown-item">Sogra</a>
					<div class="dropdown-divider"></div>
					<strong class="d-block ff-title mb-2">Prints</strong>
					<a href="#" class="dropdown-item active">Foto Digital</a>
					<a href="#" class="dropdown-item">Foto tamanho 10x25cm</a>
					<a href="#" class="dropdown-item">Foto tamanho 26x36cm</a>
					<a href="#" class="btn btn-block btn-outline-primary">Criar Seleção</a>
				</div>
			</div>
			<div class="top-right text-white">
				<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item">Comentar</a>
					<a href="#" class="dropdown-item">Compartilhar</a>
					<a href="#" class="dropdown-item">Curtir</a>
					<a href="#" class="dropdown-item">Copiar link</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-item-danger">Ocultar foto</a>
				</div>
			</div>
			<div class="bottom-right text-white">
				<!-- Exemplo de foto curtida
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Descurtir foto"><i class="fa fa-heart text-danger"></i></a>
				-->
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Curtir foto"><i class="far fa-heart"></i></a>
				<a href="images/foto-2.jpg" class="btn btn-square btn-transparent ml-1" data-galeria-custom="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a>
			</div>
		</div>
	</li>
	<li class="grid-item m-1">
		<div class="item-foto dropdown">
			<div class="img">
				<img src="images/foto-3.jpg" alt="Foto 3" title="Foto 3">
			</div>
			<div class="top-left d-flex flex-column align-items-start text-white">
				<div class="custom-control custom-checkbox number number-sm theme-1 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-3-categoria-album" name="categorias">
				  <label class="custom-control-label" data-for=".foto-3-categoria-album"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-3-categoria-album">Seleção Álbum</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-2 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-3-categoria-sogra" name="categorias">
				  <label class="custom-control-label" data-for=".foto-3-categoria-sogra"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-3-categoria-sogra">Seleção Sogra</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-white label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-3-categoria-foto-10x15" name="categorias">
				  <label class="custom-control-label" data-for=".foto-3-categoria-foto-10x15"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-3-categoria-foto-10x15">Foto 10x15cm</label>
				</div>
				<button class="btn btn-sm btn-transparent my-2" data-toggle="dropdown"><i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos</button>
				<div class="dropdown-menu menu-clickable">
					<h4 class="dropdown-header">Outros Produtos</h4>
					<strong class="d-block ff-title mb-2">Seleção</strong>
					<a href="#" class="dropdown-item active">Álbum</a>
					<a href="#" class="dropdown-item">Sogra</a>
					<div class="dropdown-divider"></div>
					<strong class="d-block ff-title mb-2">Prints</strong>
					<a href="#" class="dropdown-item active">Foto Digital</a>
					<a href="#" class="dropdown-item">Foto tamanho 10x25cm</a>
					<a href="#" class="dropdown-item">Foto tamanho 26x36cm</a>
					<a href="#" class="btn btn-block btn-outline-primary">Criar Seleção</a>
				</div>
			</div>
			<div class="top-right text-white">
				<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item">Comentar</a>
					<a href="#" class="dropdown-item">Compartilhar</a>
					<a href="#" class="dropdown-item">Curtir</a>
					<a href="#" class="dropdown-item">Copiar link</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-item-danger">Ocultar foto</a>
				</div>
			</div>
			<div class="bottom-right text-white">
				<!-- Exemplo de foto curtida
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Descurtir foto"><i class="fa fa-heart text-danger"></i></a>
				-->
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Curtir foto"><i class="far fa-heart"></i></a>
				<a href="images/foto-grande.jpg" class="btn btn-square btn-transparent ml-1" data-galeria-custom="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a>
			</div>
		</div>
	</li>
	<li class="grid-item m-1">
		<div class="item-foto dropdown">
			<div class="img">
				<img src="images/foto-4.jpg" alt="Foto 4" title="Foto 4">
			</div>
			<div class="top-left d-flex flex-column align-items-start text-white">
				<div class="custom-control custom-checkbox number number-sm theme-1 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-4-categoria-album" name="categorias">
				  <label class="custom-control-label" data-for=".foto-4-categoria-album"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-4-categoria-album">Seleção Álbum</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-2 label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-4-categoria-sogra" name="categorias">
				  <label class="custom-control-label" data-for=".foto-4-categoria-sogra"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-4-categoria-sogra">Seleção Sogra</label>
				</div>
				<div class="custom-control custom-checkbox number number-sm theme-white label-right px-2">
				  <input type="checkbox" class="custom-control-input" class="foto-4-categoria-foto-10x15" name="categorias">
				  <label class="custom-control-label" data-for=".foto-4-categoria-foto-10x15"><i class="fal fa-check"></i></label>
				  <label class="custom-control-text" data-for=".foto-4-categoria-foto-10x15">Foto 10x15cm</label>
				</div>
				<button class="btn btn-sm btn-transparent my-2" data-toggle="dropdown"><i class="fa fa-fw fa-shopping-cart mr-3"></i> Outros produtos</button>
				<div class="dropdown-menu menu-clickable">
					<h4 class="dropdown-header">Outros Produtos</h4>
					<strong class="d-block ff-title mb-2">Seleção</strong>
					<a href="#" class="dropdown-item active">Álbum</a>
					<a href="#" class="dropdown-item">Sogra</a>
					<div class="dropdown-divider"></div>
					<strong class="d-block ff-title mb-2">Prints</strong>
					<a href="#" class="dropdown-item active">Foto Digital</a>
					<a href="#" class="dropdown-item">Foto tamanho 10x25cm</a>
					<a href="#" class="dropdown-item">Foto tamanho 26x36cm</a>
					<a href="#" class="btn btn-block btn-outline-primary">Criar Seleção</a>
				</div>
			</div>
			<div class="top-right text-white">
				<button class="btn btn-square btn-transparent" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item">Comentar</a>
					<a href="#" class="dropdown-item">Compartilhar</a>
					<a href="#" class="dropdown-item">Curtir</a>
					<a href="#" class="dropdown-item">Copiar link</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-item-danger">Ocultar foto</a>
				</div>
			</div>
			<div class="bottom-right text-white">
				<!-- Exemplo de foto curtida
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Descurtir foto"><i class="fa fa-heart text-danger"></i></a>
				-->
				<a href="#" class="btn btn-square btn-transparent mb-0" title="Curtir foto"><i class="far fa-heart"></i></a>
				<a href="images/foto-grande.jpg" class="btn btn-square btn-transparent ml-1" data-galeria-custom="galeria" title="Ver foto"><i class="far fa-expand-arrows-alt"></i></a>
			</div>
		</div>
	</li>
</ul>
*/

?>