$(window).load(function() {
  if($('body').hasClass('loading')){
    $('body').addClass('loaded').removeClass('loading');
  }
  // Ratio Img
  $('.ratio-img img').each(function(i){
    var imgW = this.width,
        imgH = this.height,
        divW = $(this).parent('.ratio-img').width(),
        divH = $(this).parent('.ratio-img').height();

    var imgClass = (imgW / imgH < divW / divH ? 'tall' : 'wide');
    $(this).addClass(imgClass);

  });
});

$(document).ready(function(){
  $('body').addClass('loading');
  
  //add BT DD show event
  $(".dropdown").on("show.bs.dropdown", function() {
    var $btnDropDown = $(this).find("[data-toggle='dropdown']");
    var $listHolder = $(this).find(".dropdown-menu");
    //reset position property for DD container
    $(this).addClass("static");
    $listHolder.css({
      "top": ($btnDropDown.offset().top + $btnDropDown.outerHeight(true)) + "px",
      "left": $btnDropDown.offset().left + "px"
    });
    $listHolder.data("open", true);
  });
  //add BT DD hide event
  $(".dropdown").on("hidden.bs.dropdown", function() {
    var $listHolder = $(this).find(".dropdown-menu");
    $listHolder.data("open", false);
    $(this).removeClass("static");
  });

  $('.item-foto').each(function(index, el) {
    var this_inputs = $(el).find('input[type="checkbox"],input[type="radio"]');
    
    this_inputs.change(function(event) {
      if(this_inputs.is(':checked')){
        $(el).addClass('active');
      }
      else {
        $(el).removeClass('active');
      }
    });
  });

  // stick menu
  var height_sticky = $('#topo').height();
      $('#topo').height(height_sticky);

  $(window).on('scroll', function () {    

    var top_scroll = $(this).scrollTop();
    var top_header = $('#topo').offset().top;

    if(top_scroll > top_header) $('#topo').addClass('is-sticky');
    else{ $('#topo').removeClass('is-sticky'); }
  });

  $('.input-number button').click(function(event) {
      var button_is_plus = ($(this).hasClass('btn-plus') ? true : false),
          input_number = $(this).parent().siblings('input[type="number"]'),
          new_val = (button_is_plus ? Number(input_number.val()) + 1 : Number(input_number.val()) - 1);

      input_number.val((new_val >= 0 ? new_val : 0));
  });
  // -- fim stick

  function verifyEdges(elem){
    var totalW = 0;
    $(elem).find('.filters-item').each(function(index, el) {
      totalW += $(el).outerWidth();
    });

    //console.log($.type(elem.scrollLeft));
    if ($.type(elem.scrollLeft) !== 'number') {
      elem.scrollLeft = 0;
    }

    if(elem.scrollLeft == 0){
      $(elem).parent().addClass('left');
    }
    else{
      $(elem).parent().removeClass('left');
    }
    if(totalW < $(elem).outerWidth() || elem.scrollLeft + $(elem).outerWidth() >= totalW){
      $(elem).parent().addClass('right');
    }
    else{
      $(elem).parent().removeClass('right');
    }

  }

  $('.filters-content').on('mousewheel mouseup mousemove', function(e, delta) {
    if(e.type === 'mousewheel'){
      this.scrollLeft -= (delta * 20);
    }
    
    verifyEdges(this);
    
    if(e.type === 'mousewheel'){
      e.preventDefault();
    }
  });

  verifyEdges($('.filters-content'));

  $('.flex-images').flexImages({
      container: '.item-foto',
      rowHeight: 320,
      truncate: false
  });

  $('.modal').on('show.bs.modal shown.bs.modal', function (e) {
    var $grid = $('.flex-images').flexImages({
      container: '.item-foto',
      rowHeight: 320,
      truncate: false
    });
    if($('.fancybox-custom-layout').length > 0){
      $(this).css('zIndex', '99999');
      $('.modal-backdrop').css('zIndex', '99998');
    }
    else {
      $(this).css('zIndex', '99991');
      $('.modal-backdrop').css('zIndex', '99990');
    }
  });

  $('body').on('click', 'label[data-for]', function(){    
    var click = $(this).data('for');
    $(click).trigger('click');
  });

  if($('body').hasClass('nao-logado')){
    $('#modal-login').modal({
      keyboard: false,
      backdrop: 'static'
    });
  }

});