<?php

    // echo "<pre>";
    // print_r($contato);
    // exit();

?>

<footer id="rodape" class="text-white ovh">
    <div class="container pt-section pb-5">
        <div class="row">
            <div class="col-lg-3 mb-5 mb-lg-0 wow fadeInUp">
                <img src="assets/site/images/logo-branco.png" alt="<?php echo $configuracao->titulo; ?>" title="<?php echo $configuracao->titulo; ?>" class="img-fluid mb-5">
                <p><?php echo $contato->content('endereco'); ?></p>
                <p><?php echo $contato->content('telefones'); ?></p>
                <a href="mailto:<?php echo $contato->content('email'); ?>" target="_blank"><?php echo $contato->content('email'); ?></a>
                <?php
                    if($configuracao->sociais){
                ?>
                    <div class="fz-2x mt-3">
                        
                <?php
                        foreach ($configuracao->sociais as $key => $value) {
                ?>
                        <a href="<?php echo $value->link; ?>" target="_blank" class="circle-icon circle-icon-hover" title="<?php echo $value->plataforma; ?>"><i class="fab fa-fw fa-<?php echo $value->plataforma; ?>"></i></a>
                <?php
                        }
                ?>
                    </div>
                <?php
                    }
                ?>
            </div>
            <div class="col-lg-2 col-md-4 col-6 offset-lg-2 mb-5 mb-md-0 wow fadeInUp" data-wow-delay=".2s">
                <h3 class="fw-2x fz-1x mt-3 mb-lg-5 mb-3 color-1">Empresa</h3>
                <ul class="links-list flex-list">
                    <li class="w-100"><a href="inicio">Home</a></li>
                    <li class="w-100"><a href="empresa">Quem Somos</a></li>
                    <li class="w-100"><a href="parceiros">Parceiros</a></li>
                    <li class="w-100"><a href="unidades">Unidades</a></li>
                    <li class="w-100"><a href="#" target="_blank">Franquia</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-4 col-6 order-md-2 mb-5 mb-md-0 wow fadeInUp" data-wow-delay=".6s">
                <h3 class="fw-2x fz-1x mt-3 mb-lg-5 mb-3 color-1">Ajuda</h3>
                <ul class="links-list flex-list">
                    <li class="w-100"><a href="duvidas">Tire suas dúvidas</a></li>
                    <li class="w-100"><a href="informacoes">Informações</a></li>
                    <li class="w-100"><a href="contato">Contato</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-4 col-12 mb-5 mb-md-0 wow fadeInUp" data-wow-delay=".4s">
                <h3 class="fw-2x fz-1x mt-3 mb-lg-5 mb-3 color-1">Serviços</h3>
                <ul class="links-list flex-list">
                    <li class="w-100"><a href="servicos/">Crédito Pessoal</a></li>
                    <li class="w-100"><a href="servicos/">Crédito Consignado</a></li>
                    <li class="w-100"><a href="servicos/">Crédito Privado</a></li>
                    <li class="w-100"><a href="servicos/">Financiamento Auto</a></li>
                    <li class="w-100"><a href="servicos/">Financiamento Imóvel</a></li>
                    <li class="w-100"><a href="servicos/">Seguro Auto</a></li>
                    <li class="w-100"><a href="servicos/">Seguro Vida</a></li>
                    <li class="w-100"><a href="servicos/">Consórcio Auto</a></li>
                    <li class="w-100"><a href="servicos/">Consórcio Imóvel</a></li>
                    <li class="w-100"><a href="servicos/">Consórcio Serviços</a></li>
                    <li class="w-100"><a href="servicos/">Cartão de Crédito</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <hr class="row my-0 dark">
    </div>
    <div class="container py-5 font-14 wow fadeInUp" data-wow-offset="0">
        <div class="row align-items-center justify-content-between text-center text-left-sm">
            <div class="col-sm-auto opacity-50">
                <span>© 2019 Direitos Reservados <!-- - <a href="#" target="_blank">Termos de Uso</a> - <a href="#" target="_blank">Política de Privacidade</a> --></span>
            </div>
            <div class="col-sm-auto mt-2 mt-sm-0">
                <a href="http://www.8pdev.studio" target="_blank" class="link-8p">Desenvolvido por 8Pdev.studio</a>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade modal-side" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fal fa-fw fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="slick-modal">
                    <div>
                        <div class="text-center">
                            <h3 class="fz-2x color-1 ff-title mb-1">Recuperar Senha</h3>
                            <p>Lembrou sua senha? <a onclick="slickNext('.slick-modal')" class="text-underline">Acesse aqui</a>.</p>
                        </div>
                        <form id="form-recuperar-senha" class="text-left needs-validation mt-5" novalidate>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" placeholder="Informe seu e-mail" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary btn-block">Recuperar senha</button>
                            </div>
                        </form>
                    </div>
                    <div>
                        <div class="text-center">
                            <h3 class="fz-2x color-1 ff-title mb-1">Acesse seu certificado</h3>
                            <p>Não possui cadastro? <a onclick="slickNext('.slick-modal')" class="text-underline">Acesse aqui</a>.</p>
                        </div>
                        <form id="form-login" class="text-left needs-validation mt-5" novalidate>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" placeholder="Informe seu e-mail" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" name="senha" class="form-control" placeholder="Informe sua senha" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="form-group">
                                <div class="d-flex align-items-center justify-content-between">
                                    <a onclick="slickPrev('.slick-modal')" class="text-underline">Esqueceu sua senha?</a>
                                    <button type="submit" class="btn btn-secondary m-0">Entrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>
                        <div class="text-center">
                            <h3 class="fz-2x color-1 ff-title mb-1">Cadastra-se</h3>
                            <p>Já possui cadastro? <a onclick="slickPrev('.slick-modal')" class="text-underline">Acesse aqui</a>.</p>
                        </div>
                        <form id="form-cadastrar" class="text-left needs-validation mt-5" novalidate>
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" placeholder="Informe seu nome" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" placeholder="Informe seu e-mail" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="form-group">
                                <label>Celular</label>
                                <input type="phone" name="celular" class="form-control" placeholder="Informe seu Whatsapp ou telefone" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" name="senha" class="form-control" placeholder="Informe sua senha" required>
                                <small class="invalid-feedback">Campo obrigatório</small>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="cadastrar-feedback text-danger"></div>
                                </div>
                                <div class="col-auto">                                    
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-secondary btn-cadastrar">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-side" id="modal-certificado" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-1 text-white f-flex justify-content-between">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fal fa-fw fa-times"></i></button>
                <span class="hello">
                    <p class="d-inline m-0" style="line-height: 1;">
                        <?php echo ($this->session->userdata('usuario')) ? $this->session->userdata('usuario')['nome'] : NULL; ?>
                    </p>
                    <i class="fa fa-user-circle ml-2"></i>
                    <small><a href="main/sair" class="d-inline ml-2" style="color: #fff;">Sair</a></small>
                </span>
            </div>
            <div class="modal-body">
                <h3 class="fz-2x color-1 ff-title mb-4"><i class="far fa-award color-3 mr-1"></i> Certificados</h3>
                <ul class="lista-certificados">
                    <li>
                        <div class="spinner-grow" role="status">
                            <span class="sr-only">Carregando...</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>