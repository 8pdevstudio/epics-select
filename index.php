<?php
	
include('helper.php');

?>

<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#333333">
    <meta name="msapplication-navbutton-color" content="#333333">
    <meta name="apple-mobile-web-app-status-bar-style" content="#333333">
    <title>Ana Letícia e Luiz - Epics Select</title>

    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="fonts/icones/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <!-- <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css"/> -->
    <link rel="stylesheet" href="css/main.css" type="text/css">

    <link rel="stylesheet" href="assets/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/galeria-custom.css" type="text/css">
    
</head>
<!-- Para aparecer o modal de login, adiciona a classe "nao-logado" no <body> -->
<body <?php /*echo "class='nao-logado'";*/ ?>>

	<section id="banner" style="background-image: url(images/banner.jpg);">
		<div class="container h-100">
			<div class="content text-center text-white h-100 d-flex flex-column justify-content-between align-items-center col-md-8 offset-md-2">
				<img src="images/logo-branco.png" alt="Evandro Rocha Fotógrafo" title="Evandro Rocha Fotógrafo">
				<div class="pt-5">
					<h1 class="title title-1 mb-0">Ana Letícia e Luiz</h1>
					<small>27.5.2019</small>
					<br>
					<a href="#" class="btn btn-transparent mt-80" data-autoplay-galeria data-index="0"><i class="fa fa-play mr-2"></i> slideshow</a>
				</div>
			</div>
		</div>
	</section>

	<main>
		<div class="body-content">

			<header id="topo">
				<div class="container-fluid bg-white px-0">
					<div class="container">
						<div class="row mx-0 py-3 align-items-center justify-content-between">
							<div class="col-auto d-none d-md-block">
								<h2 class="title title-2 mb-1">Ana Letícia e Luiz</h2>
								<span class="text-muted">Fotos por: Evandro Rocha</span>
							</div>
							<div class="col-auto d-md-none pr-0">
								<a href="" class="btn btn-square btn-transparent circle"><i class="fa fa-arrow-to-bottom"></i></a>
							</div>
							<div class="col text-right">
								<form class="d-inline-block mr-1 mr-md-3">
									<div class="custom-control custom-checkbox number theme-1 px-1 px-md-2">
									  <input type="checkbox" class="custom-control-input" id="categoria-album" name="categorias">
									  <label class="custom-control-label" for="categoria-album">123</label>
									  <label class="custom-control-text" for="categoria-album">Álbum</label>
									</div>
									<div class="custom-control custom-checkbox number theme-2 px-1 px-md-2 d-none d-md-inline-block">
									  <input type="checkbox" class="custom-control-input" id="categoria-sogra" name="categorias">
									  <label class="custom-control-label" for="categoria-sogra">123</label>
									  <label class="custom-control-text" for="categoria-sogra">Sogra</label>
									</div>
									<div class="custom-control custom-checkbox number px-1 px-md-2 d-none d-md-inline-block">
									  <input type="checkbox" class="custom-control-input" id="categoria-foto-10x15" name="categorias">
									  <label class="custom-control-label" for="categoria-foto-10x15">123</label>
									  <label class="custom-control-text" for="categoria-foto-10x15">Foto 10x15</label>
									</div>
								</form>
								<a href="" class="btn btn-square btn-transparent circle ml-4 d-none d-md-inline-block"><i class="fa fa-arrow-to-bottom"></i></a>
								<button class="btn btn-square btn-transparent circle ml-1 d-none d-md-inline-block" data-toggle="modal" data-target="#modal-search"><i class="fa fa-search"></i></button>
								<button class="btn btn-lg btn-primary my-0 mr-md-3" data-toggle="modal" data-target="#modal-orcamento">Finalizar</button>
								<div class="dropdown d-inline-block">
									<button class="ratio-img ratio-img-avatar ratio-img-avatar-3 d-none d-md-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<!-- Se tiver imagem lista aqui -->
										<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
										<span>AL</span>
									</button>
									<button class="btn btn-square btn-transparent d-md-none m-0" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
									<div class="dropdown-menu dropdown-menu-right menu-clickable">
										<div class="d-flex mx-n3 align-items-center">
											<div class="col-auto">
												<div class="ratio-img ratio-img-avatar ratio-img-avatar-3">
													<!-- Se tiver imagem lista aqui -->
													<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
													<span>AL</span>
												</div>
											</div>
											<div class="col pl-0 ff-title">
												<strong class="d-block">Ana Letícia</strong>
												<span class="text-muted">analeticia@epics.com.br</span>
											</div>
										</div>
										<div class="dropdown-divider"></div>
										<a href="" class="dropdown-item">Configuração de conta</a>
										<a href="" class="dropdown-item">Minhas galerias</a>
										<a href="" class="dropdown-item">Compras</a>
										<a href="" class="dropdown-item">Ajuda</a>
										<a href="" class="dropdown-item dropdown-item-danger">Sair</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div id="filtros" class="pb-3 pt-0 pt-md-3">
				<div class="container px-0 px-md-3">
					<form class="row mx-0 justify-content-between">
						<div class="col-md-6 pr-lg-5 pl-0 pr-0 pr-md-3">
							<div class="filters-list">
								<div class="filters-content dragscroll">
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox">
										  <input type="checkbox" class="custom-control-input" id="filtro-todas" name="filtros">
										  <label class="btn custom-control-label" for="filtro-todas">Todas</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox">
										  <input type="checkbox" class="custom-control-input" id="filtro-selecionadas" name="filtros">
										  <label class="btn custom-control-label" for="filtro-selecionadas">Selecionadas</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox">
										  <input type="checkbox" class="custom-control-input" id="filtro-curtidas" name="filtros">
										  <label class="btn custom-control-label" for="filtro-curtidas"><i class="fa fa-heart text-danger mr-2"></i> Mais curtidas</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 pl-lg-5 pr-0 d-none d-md-block">
							<div class="filters-list text-right">
								<div class="filters-content dragscroll">
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox transparent">
										  <input type="checkbox" class="custom-control-input" id="filtro-making-of" name="filtros">
										  <label class="btn custom-control-label" for="filtro-making-of">making of</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox transparent">
										  <input type="checkbox" class="custom-control-input" id="filtro-cerimonia" name="filtros">
										  <label class="btn custom-control-label" for="filtro-cerimonia">cerimonia</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox transparent">
										  <input type="checkbox" class="custom-control-input" id="filtro-decoracao" name="filtros">
										  <label class="btn custom-control-label" for="filtro-decoracao">decoração</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox transparent">
										  <input type="checkbox" class="custom-control-input" id="filtro-tag1" name="filtros">
										  <label class="btn custom-control-label" for="filtro-tag1">Tag 1</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox transparent">
										  <input type="checkbox" class="custom-control-input" id="filtro-tag2" name="filtros">
										  <label class="btn custom-control-label" for="filtro-tag2">Tag 2</label>
										</div>
									</div>
									<div class="filters-item px-2">
										<div class="custom-control custom-checkbox transparent">
										  <input type="checkbox" class="custom-control-input" id="filtro-tag3" name="filtros">
										  <label class="btn custom-control-label" for="filtro-tag3">Tag 3</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="m-2 mb-5">
				<?php

					include('grid.php');

				?>
			</div>

			<div class="container mt-4">
				<nav aria-label="Page navigation example">
					<ul class="pagination justify-content-end">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true"><i class="far fa-chevron-left"></i></span>
							</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item active"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true"><i class="far fa-chevron-right"></i></span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>

		<footer id="rodape" class="pt-5">
			<div class="container">
				<div class="row align-items-center pb-5">
					<div class="col-md-4 mb-5 mb-md-0 text-center text-md-left">
						<div class="ff-title mb-4">
							<strong class="d-block mb-2">Ana Letícia e Luiz</strong>
							<small class="d-block text-muted">12.2.2019</small>
						</div>
						<div class="mb-4">
							<p>Foi uma festa cheia de energia, com certeza os noivos se divertiram muito junto com as pessoas queridas.</p>
						</div>
						<a href="#" target="_blank" class="row justify-content-center text-muted">
							<div class="col-auto">
								<i class="fa fa-map-marker-alt"></i>
							</div>
							<div class="col-auto col-md pl-0 text-left">
								<p>Buffet Felix Petrolli - São José do Rio Preto</p>
							</div>
						</a>
					</div>
					<div class="col-md-4 mb-5 mb-md-0">
						<div class="row justify-content-center align-items-center text-center text-xl-left">
							<div class="col-auto mb-3 mb-xl-0">
								<img src="images/logo.png" alt="Evandro Rocha" title="Evandro Rocha" class="img-fluid">
							</div>
							<div class="col-12 col-xl-auto pl-0">
								<span>Fotos por:</span>
								<h3 class="title title-2 mt-0 mb-2">EVANDRO ROCHA</h3>
								<a href="http://www.evandrorocha.com.br" target="_blank" class="text-muted">www.evandrorocha.com.br</a>
								<div class="mt-2 mx-n2">
									<a href="#" target="_blank" class="text-muted p-2"><i class="fab fa-whatsapp fz-2x"></i></a>
									<a href="#" target="_blank" class="text-muted p-2"><i class="fab fa-instagram fz-2x"></i></a>
									<a href="#" target="_blank" class="text-muted p-2"><i class="fab fa-facebook fz-2x"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<h3 class="ff-title fz-2x">Gostou das fotos?</h3>
						<button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#modal-orcamento">Peça um orçamento</button>
					</div>
				</div>
				<div class="row py-3 text-center">
					<div class="col">
						<a href="#" target="_blank"><strong>EPICS</strong> | Soluções online para fotógrafos e videomakers</a>
					</div>
				</div>
			</div>
		</footer>
	</main>

	<form id="comentario" class="fixed-comment d-none d-md-block">
		<div class="input-group comment">
			<span class="ratio-img ratio-img-avatar ratio-img-avatar-3">
				<!-- Se tiver imagem lista aqui -->
				<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia">
				<span>AL</span>
			</span>
			<input type="text" class="form-control" placeholder="Deixe um comentário...">
		</div>
	</form>

	<div class="modal fade modal-overlay" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content text-center text-white h-100 flex-row align-items-center px-3">
	            <div class="modal-body container px-0">
					<img src="images/logo-branco.png" alt="Evandro Rocha Fotógrafo" title="Evandro Rocha Fotógrafo">
					<h1 class="title title-1 mb-0 mt-5">Ana Letícia e Luiz</h1>
					<small>27.5.2019</small>
					<div class="maw-320 mx-auto mt-5">
						<a href="" class="btn btn-block btn-facebook mb-3">Entrar com o Facebook <i class="fab fa-facebook-f ml-3"></i></a>
		                <div class="row justify-content-between">
		                	<div class="col-auto pl-1">
		                		<a href="" class="btn btn-transparent">Entrar com e-mail</a>
		                	</div>
		                	<div class="col-auto pr-1">
		                		<a href="" class="btn btn-transparent">Criar conta</a>
		                	</div>
		                </div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade modal-overlay bg-white" id="modal-search" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
	    <div class="modal-dialog" role="document">
        	<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	        <div class="modal-content h-100 flex-row">
	            <div class="modal-body container">
					<form>
						<div class="input-group input-group-icon">
							<i class="far fa-search"></i>
							<input type="text" class="form-control form-control-sm" placeholder="Pesquisar suas fotos" required="">
						</div>
					</form>
					<ul class="row align-items-center mx-0 mt-5">
						<li class="lh-1"><i class="fal fa-fw fa-user fz-2x"></i></li>
						<li class="pl-3">
							<span class="ratio-img ratio-img-avatar ratio-img-avatar-3">
								<!-- Se tiver imagem lista aqui -->
								<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia">
								<span>AL</span>
							</span>
						</li>
						<li class="pl-3">
							<span class="ratio-img ratio-img-avatar ratio-img-avatar-3">
								<!-- Se tiver imagem lista aqui -->
								<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia">
								<span>AL</span>
							</span>
						</li>
						<li class="pl-3">
							<span class="ratio-img ratio-img-avatar ratio-img-avatar-3">
								<!-- Se tiver imagem lista aqui -->
								<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia">
								<span>AL</span>
							</span>
						</li>
						<li class="pl-3">
							<a href="" class="btn btn-border btn-square circle"><i class="fal fa-plus"></i></a>
						</li>
					</ul>
					<ul class="row align-items-center mx-0 mt-5">
						<li class="lh-1"><i class="fal fa-fw fa-folder-open fz-2x"></i></li>
						<li class="pl-3">
							<a href="" class="btn">cerimonia</a>
						</li>
						<li class="pl-3">
							<a href="" class="btn">cerimonia</a>
						</li>
						<li class="pl-3">
							<a href="" class="btn">cerimonia</a>
						</li>
					</ul>
					<ul class="row align-items-center mx-0 mt-5">
						<li class="lh-1"><i class="fal fa-fw fa-trash-alt fz-2x"></i></li>
						<li class="pl-3">
							<a href="" class="btn">mostrar fotos ocultas</a>
						</li>
					</ul>
					<!-- Aqui é a outra página -->
					<div class="d-flex align-items-center mt-5">
						<span class="ratio-img ratio-img-avatar ratio-img-avatar-3">
							<!-- Se tiver imagem lista aqui -->
							<img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia">
							<span>CP</span>
						</span>
						<span class="ml-3 ff-title">Carlos Pereira</span>
						<span class="ml-3"><i class="far fa-check"></i></span>
					</div>
					<div class="mt-5">
						<strong class="d-block ff-title mb-3 text-uppercase">Nos álbuns</strong>
						<div class="row my-n3">
							<a href="" class="item-album">
								<div class="ratio-img ratio-img-1by1">
									<img src="images/foto-2.jpg" alt="Álbum Cerimonia" title="Álbum Cerimonia">
								</div>
								<span>Cerimonia</span>
							</a>
							<a href="" class="item-album">
								<div class="ratio-img ratio-img-1by1">
									<img src="images/foto-2.jpg" alt="Álbum Cerimonia" title="Álbum Cerimonia">
								</div>
								<span>Cerimonia</span>
							</a>
							<a href="" class="item-album">
								<div class="ratio-img ratio-img-1by1">
									<img src="images/foto-2.jpg" alt="Álbum Cerimonia" title="Álbum Cerimonia">
								</div>
								<span>Cerimonia</span>
							</a>
						</div>
					</div>
					<div class="mt-5">
						<strong class="d-block ff-title mb-3 text-uppercase">Nas fotos</strong>
						<?php // include('grid.php') ?>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="modal-orcamento" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
	        <div class="modal-content flex-row align-items-center">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body container modal-xl">
	            	<h3 class="title title-2 mb-5 text-center">ADICIONAR AO CARRINHO</h3>
	            	<div class="row">
	            		<div class="col-md-4 text-center text-md-left mb-5 mb-md-0">
	            			<img src="images/foto-orcamento.jpg" alt="Pacote promocional com 10 fotos" class="img-fluid d-block mx-auto">
	            			<h3 class="title title-3 mt-4">DESCRIÇÃO DO PRODUTO</h3>
	            			<div class="text-muted">
	            				<p>Foto 4 x 6</p>
								<p>Papel fotografico fosco</p>
								<p>diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
								<p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur</p>
	            			</div>
	            		</div>
	            		<div class="col-lg-7 col-md-8 offset-lg-1">
	            			<div class="limit-table">
		            			<div class="table-responsive">
		            				<table class="table table-borderless table-hover">
										<thead>
											<tr>
												<th scope="col" class="w-100">Pacotes</th>
												<th scope="col" class="minw-7r">Preço</th>
												<th scope="col" class="minw-10r">Quantidade</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="minw-7r">Pacote promocional com 10 fotos</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
		            			</div>
		            			<div class="table-responsive border-top">
		            				<table class="table table-borderless table-hover">
										<thead>
											<tr>
												<th scope="col" class="w-100">Prints</th>
												<th scope="col" class="minw-7r">Preço</th>
												<th scope="col" class="minw-10r">Quantidade</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="minw-7r">Foto 4 x 6</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="minw-7r">Foto 8 x 12</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="minw-7r">Foto 16 x 24</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="minw-7r">Foto 20 x 30</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="minw-7r">Foto 24 x 36</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="minw-7r">Foto 32 x 48</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="minw-7r">Foto 38 x 56</td>
												<td class="minw-7r">R$ 10,00</td>
												<td class="minw-10r">
													<div class="input-group input-number">
														<div class="input-group-prepend">
															<button class="btn btn-square btn-transparent btn-minus" type="button"><i class="far fa-minus"></i></button>
														</div>
														<input type="number" class="form-control" value="1" min="0" required="">
														<div class="input-group-append">
															<button class="btn btn-square btn-transparent btn-plus" type="button"><i class="far fa-plus"></i></button>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
		            			</div>
	            			</div>
	            			<div class="text-center mt-5 mx-n4 mx-sm-0">
	            				<button class="btn btn-outline-primary mb-0" data-dismiss="modal">Continuar comprando</button>
	            				<a href="" class="btn btn-primary">Finalizar</a>
	            			</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="modal-compatilhar" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body">
					<h4 class="dropdown-header">Compartilhar</h4>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Digite um nome ou e-mail">
					</div>
					<div class="row text-center lh-1">
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								<!-- Se tiver imagem lista aqui -->
								<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								<!-- Se tiver imagem lista aqui -->
								<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								<!-- Se tiver imagem lista aqui -->
								<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								<!-- Se tiver imagem lista aqui -->
								<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								<!-- Se tiver imagem lista aqui -->
								<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
						<a href="" class="col-4 p-3">
							<div class="ratio-img ratio-img-avatar ratio-img-avatar-3 mb-2 d-block mx-auto">
								<!-- Se tiver imagem lista aqui -->
								<!-- <img src="images/perfil.jpg" alt="Ana Letícia" title="Ana Letícia"> -->
								<span>AL</span>
							</div>
							<span>Jaqueline cunha</span>
						</a>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- Exemplo de modais, remover os botões depois -->
	<button type="button" class="btn" data-toggle="modal" data-target="#modal-1">modal 1</button>
	<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body text-center">
					<h4 class="dropdown-header">Você já terminou a seleção?</h4>
					<p>Vimos que você não selecionou fotos para o carrinho, gostaria de finalizar mesmo assim?</p>
					<a href="" class="btn btn-outline-primary">Add fotos ao carrinho</a>
					<a href="" class="btn btn-lg btn-primary">Confirmar</a>
	            </div>
	        </div>
	    </div>
	</div>

	<button type="button" class="btn" data-toggle="modal" data-target="#modal-2">modal 2</button>
	<div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body text-center">
					<h4 class="dropdown-header">Você ainda não terminou a seleção, quer sair mesmo assim?</h4>
					<p>O prazo para selecionar as fotos é de <strong>xx dias</strong></p>
					<p>*Caso precise sair, as fotos selecionadas ficaram salvas para a próxima seção.</p>
					<a href="" class="btn btn-outline-primary">Continuar seleção</a>
					<a href="" class="btn btn-lg btn-primary">Sair</a>
	            </div>
	        </div>
	    </div>
	</div>

	<button type="button" class="btn" data-toggle="modal" data-target="#modal-3">modal 3</button>
	<div class="modal fade" id="modal-3" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body text-center">
					<h4 class="dropdown-header">Carrinho</h4>
					<p>Gostaria de adicionar mais fotos ao seu carrinho?</p>
					<a href="" class="btn btn-outline-primary">Add fotos ao carrinho</a>
					<a href="" class="btn btn-primary">Confirmar seleção</a>
	            </div>
	        </div>
	    </div>
	</div>

	<button type="button" class="btn" data-toggle="modal" data-target="#modal-4">modal 4</button>
	<div class="modal fade" id="modal-4" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body text-center">
					<h4 class="dropdown-header">Finalizar Seleção</h4>
					<div class="card text-left p-3 mb-4">
						<div class="row m-0 align-items-center">
							<div class="col-auto p-0">
								<i class="fal fa-fw fa-check text-success fz-2x"></i>
							</div>
							<div class="col pr-0 pl-4">
								<h4 class="fz-1x mb-0">Seleção do Álbum</h4>
								<small>Todas as foto do Álbum foram selecionadas</small>
							</div>
						</div>
					</div>
					<p>Gostaria de adicionar fotos ao seu carrinho?</p>
					<a href="" class="btn btn-outline-primary">Ver carrinho</a>
					<a href="" class="btn btn-lg btn-primary">Finalizar</a>
	            </div>
	        </div>
	    </div>
	</div>

	<button type="button" class="btn" data-toggle="modal" data-target="#modal-5">modal 5</button>
	<div class="modal fade" id="modal-5" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
        		<button class="btn btn-square btn-danger close" data-dismiss="modal"><i class="fal fa-times"></i></button>
	            <div class="modal-body text-center">
					<h4 class="dropdown-header">Finalizar Seleção</h4>
					<div class="card text-left p-3 mb-4">
						<div class="row m-0 align-items-center">
							<div class="col-auto p-0">
								<i class="fal fa-fw fa-check text-success fz-2x"></i>
							</div>
							<div class="col pr-0 pl-4">
								<h4 class="fz-1x mb-0">Seleção do Álbum</h4>
								<small>Todas as foto do Álbum foram selecionadas</small>
							</div>
						</div>
					</div>
					<div class="card p-3 mb-4">
						<p>Gostaria de adicionar fotos ao seu carrinho?</p>
					</div>
					<a href="" class="btn btn-block btn-primary">Finalizar</a>
	            </div>
	        </div>
	    </div>
	</div>

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/dragscroll.js"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- <script type="text/javascript" src="js/jquery.fancybox.js"></script> -->
    <!-- <script type="text/javascript" src="js/jquery.sticky.js"></script> -->
    <!-- <script type="text/javascript" src="js/jquery.sticky-kit.min.js"></script> -->
    <script type="text/javascript" src="js/fontsmoothie.min.js"></script>
    <script type="text/javascript" src="js/flex-images.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript" src="assets/js/scripts/galleries.js"></script>
    <script type="text/javascript" src="assets/js/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="assets/js/swipedetect.js"></script>
    <script type="text/javascript" src="assets/js/galeria-custom.js"></script>
</body>
</html>