<header id="topo">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <input id="check-nav" type="checkbox" class="d-none"/>
            <div class="col-auto">
                <a href="inicio" class="logo">
                    <img src="<?php echo $configuracao->get_img('auto', 'logo'); ?>" alt="<?php echo $configuracao->titulo; ?>" title="<?php echo $configuracao->titulo; ?>">
                    <img src="assets/site/images/logo-branco.png" alt="<?php echo $configuracao->titulo; ?>" title="<?php echo $configuracao->titulo; ?>">
                </a>
            </div>
            <div class="col-auto p-0 z-1">
                <ul id="navbar" class="nav">
                    <li class="nav-item"><a href="inicio" class="nav-link active">Home</a></li>
                    <li class="nav-item"><a href="empresa" class="nav-link">Empresa</a></li>
                    <li class="nav-item"><a href="servicos" class="nav-link">Serviços</a></li>
                    <li class="nav-item"><a href="ajuda" class="nav-link">Ajuda</a></li>
                    <li class="nav-item"><a href="contato" class="nav-link">Contato</a></li>
                    <li class="nav-item d-lg-none"><a href="#" target="_blank" class="nav-link">Área do Cliente</a></li>
                    <li class="nav-item d-lg-none"><a href="#" target="_blank" class="nav-link button">Seja Franqueado</a></li>
                    <li class="line hidden-sm hidden-xs"></li>
                </ul>
                <label for="check-nav" class="overlay d-lg-none"></label>
            </div>
            <div class="col-auto d-none d-lg-block">
                <ul class="nav">
                    <li class="nav-item"><a href="#" target="_blank" class="nav-link button">Seja Franqueado</a></li>
                    <li class="nav-item ml-3"><a href="#" target="_blank" class="nav-link">Área do Cliente</a></li>
                </ul>
            </div>
            <div class="col-auto d-lg-none z2 z-2">
                <label for="check-nav" class="btn-menu"><span></span></label>
            </div>
        </div>
    </div>
</header>